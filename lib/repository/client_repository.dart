// coverage:ignore-file
import 'package:rent_vehicles_flutter/api_service/client_api_service.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/client.dart';

class ClientRepository {
  var clientApiService = ClientApiService();

  Future<ApiResponse> getAllClients() => clientApiService.getAllClients();

  Future<ApiResponse> getClientById(int clientId) =>
      clientApiService.getClientById(clientId);

  Future<ApiResponse> storeClient(Client client) =>
      clientApiService.storeClient(client);

  Future<ApiResponse> updateClient(Client client) =>
      clientApiService.updateClient(client);

  Future<ApiResponse> deleteClient(int clientId) =>
      clientApiService.deleteClient(clientId);
}
