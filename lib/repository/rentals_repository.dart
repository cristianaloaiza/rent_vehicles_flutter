// coverage:ignore-file
import 'package:rent_vehicles_flutter/api_service/rentals_api_service.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/rentals.dart';

class RentalsRepository {
  var rentalsApiService = RentalsApiService();

  Future<ApiResponse> getAllRentals() => rentalsApiService.getAllRentals();

  Future<ApiResponse> getRentalsById(int rentalsId) =>
      rentalsApiService.getRentalsById(rentalsId);

  Future<ApiResponse> storeRentals(Rentals rentals) =>
      rentalsApiService.storeRentals(rentals);

  Future<ApiResponse> updateRentals(Rentals rentals) =>
      rentalsApiService.updateRentals(rentals);

  Future<ApiResponse> deleteRentals(int rentalsId) =>
      rentalsApiService.deleteRentals(rentalsId);
}
