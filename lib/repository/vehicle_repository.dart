// coverage:ignore-file
import 'package:rent_vehicles_flutter/api_service/vehicle_api_service.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart';

class VehicleRepository {
  var vehicleApiService = VehicleApiService();

  Future<ApiResponse> getAllVehicles() => vehicleApiService.getAllVehicles();

  Future<ApiResponse> getVehicleById(int vehicleId) =>
      vehicleApiService.getVehicleById(vehicleId);

  Future<ApiResponse> storeVehicle(Vehicle vehicle) =>
      vehicleApiService.storeVehicle(vehicle);

  Future<ApiResponse> updateVehicle(Vehicle vehicle) =>
      vehicleApiService.updateVehicle(vehicle);

  Future<ApiResponse> deleteVehicle(int vehicleId) =>
      vehicleApiService.deleteVehicle(vehicleId);
}
