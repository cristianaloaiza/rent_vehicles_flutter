// coverage:ignore-file
import 'package:rent_vehicles_flutter/api_service/price_api_service.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/price.dart';

class PriceRepository {
  var priceApiService = PriceApiService();

  Future<ApiResponse> getAllPrices() => priceApiService.getAllPrices();

  Future<ApiResponse> getPriceById(int priceId) =>
      priceApiService.getPriceById(priceId);

  Future<ApiResponse> storePrice(Price price) =>
      priceApiService.storePrice(price);

  Future<ApiResponse> updatePrice(Price price) =>
      priceApiService.updatePrice(price);

  Future<ApiResponse> deletePrice(int priceId) =>
      priceApiService.deletePrice(priceId);
}
