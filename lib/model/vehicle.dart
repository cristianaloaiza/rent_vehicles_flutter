// coverage:ignore-file
class Vehicle {
  int? id;
  String? type;
  String? model;
  String? cylinder_capacity;
  String? colour;
  String? plate;
  String? mileage;
  String? status;
  String? observations;

  Vehicle(
      {this.id,
      this.type,
      this.model,
      this.cylinder_capacity,
      this.colour,
      this.plate,
      this.mileage,
      this.status,
      this.observations});

  factory Vehicle.fromJson(Map<String, dynamic>? parsedJson) {
    return Vehicle(
      id: parsedJson?['id'],
      type: parsedJson?['type'],
      model: parsedJson?['model'],
      cylinder_capacity: parsedJson?['cylinder_capacity'],
      colour: parsedJson?['colour'],
      plate: parsedJson?['plate'],
      mileage: parsedJson?['mileage'],
      status: parsedJson?['status'],
      observations: parsedJson?['observations'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'type': type,
        'model': model,
        'cylinder_capacity': cylinder_capacity,
        'colour': colour,
        'plate': plate,
        'mileage': mileage,
        'status': status,
        'observations': observations,
      };
}
