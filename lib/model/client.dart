// coverage:ignore-file
class Client {
  int? id;
  String? document_number;
  String? name;
  String? email;
  String? birthdate;
  String? license;
  String? status;

  Client(
      {this.id,
      this.document_number,
      this.name,
      this.email,
      this.birthdate,
      this.license,
      this.status});

  factory Client.fromJson(Map<String, dynamic>? parsedJson) {
    return Client(
      id: parsedJson?['id'],
      document_number: parsedJson?['document_number'],
      name: parsedJson?['name'],
      email: parsedJson?['email'],
      birthdate: parsedJson?['birthdate'],
      license: parsedJson?['license'],
      status: parsedJson?['status'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'document_number': document_number,
        'name': name,
        'email': email,
        'birthdate': birthdate,
        'license': license,
        'status': status,
      };
}
