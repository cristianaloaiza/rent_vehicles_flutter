// coverage:ignore-file

class ApiResponse {
  bool? status = false;
  int? statusResponse;
  String? message;
  dynamic data;

  ApiResponse({this.status, this.statusResponse, this.message, this.data});

  factory ApiResponse.fromJson(Map<String, dynamic>? json) {
    return ApiResponse(
      status: json?['status'],
      statusResponse: json?['statusResponse'],
      message: json?['message'],
      data: json?['data'],
    );
  }

  Map<String, dynamic> toJson() => {
        'status': status,
        'statusResponse': statusResponse,
        'message': message,
        'data': data,
      };
}
