// coverage:ignore-file
class Rentals {
  int? id;
  int? id_vehicle;
  int? id_client;
  int? id_price;
  String? start_date;
  String? final_date;
  double? total;
  String? status;
  String? observations;

  Rentals(
      {this.id,
      this.id_vehicle,
      this.id_client,
      this.id_price,
      this.start_date,
      this.final_date,
      this.total,
      this.status,
      this.observations});

  factory Rentals.fromJson(Map<String, dynamic>? parsedJson) {
    return Rentals(
      id: parsedJson?['id'],
      id_vehicle: parsedJson?['id_vehicle'],
      id_client: parsedJson?['id_client'],
      id_price: parsedJson?['id_price'],
      start_date: parsedJson?['start_date'],
      final_date: parsedJson?['final_date'],
      total: parsedJson?['total'],
      status: parsedJson?['status'],
      observations: parsedJson?['observations'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'id_vehicle': id_vehicle,
        'id_client': id_client,
        'id_price': id_price,
        'start_date': start_date,
        'final_date': final_date,
        'total': total,
        'status': status,
        'observations': observations,
      };
}
