// coverage:ignore-file
class Price {
  int? id;
  String? description;
  double? day_value;
  String? vehicle_type;
  String? status;

  Price(
      {this.id,
      this.description,
      this.day_value,
      this.vehicle_type,
      this.status});

  factory Price.fromJson(Map<String, dynamic>? parsedJson) {
    return Price(
      id: parsedJson?['id'],
      description: parsedJson?['description'],
      day_value: parsedJson?['day_value'],
      vehicle_type: parsedJson?['vehicle_type'],
      status: parsedJson?['status'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'description': description,
        'day_value': day_value,
        'vehicle_type': vehicle_type,
        'status': status,
      };
}
