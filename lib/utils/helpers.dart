// coverage:ignore-file
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/utils/constants.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';

class ClientHelper {
  Client client = Client();
  String defaultErrorMessage = 'Error al realizar la petición';
  String url;
  Utils utils = Utils();

  Map<String, String> headers = {
    HttpHeaders.contentTypeHeader: Constants.contentTypeHeader
  };

  ClientHelper(this.url);

  Uri _uri({String param = ''}) =>
      Uri.parse(Constants.urlBackend + url + param);

  ApiResponse _processResponse(
    Response response,
  ) {
    var apiResponse = ApiResponse();

    var body;

    try {
      body = jsonDecode(utf8.decode(response.bodyBytes));
    } catch (e) {
      body = jsonDecode(response.body);
    }

    apiResponse.statusResponse = response.statusCode;
    apiResponse.status = body?['status'] ?? false;
    apiResponse.message = body?['message'] ?? defaultErrorMessage;

    if (apiResponse.statusResponse == 200 && apiResponse.status == true) {
      apiResponse.data = body?['data'];
    }

    return apiResponse;
  }

  Future<ApiResponse> get({String param = ''}) async {
    var response = await client.get(
      _uri(param: param),
      headers: headers,
    );

    return _processResponse(response);
  }

  Future<ApiResponse> post({String param = '', body}) async {
    var response = await client.post(
      _uri(param: param),
      headers: headers,
      body: body,
    );

    return _processResponse(response);
  }

  Future<ApiResponse> put({String param = '', body}) async {
    var response = await client.put(
      _uri(param: param),
      headers: headers,
      body: body,
    );

    return _processResponse(
      response,
    );
  }

  Future<ApiResponse> delete({String param = '', body}) async {
    var response = await client.delete(
      _uri(param: param),
      headers: headers,
      body: body,
    );

    return _processResponse(response);
  }
}
