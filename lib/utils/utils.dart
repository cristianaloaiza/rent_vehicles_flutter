// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ListItem {
  String value;
  String label;

  ListItem({required this.value, required this.label});
}

class Utils {
  void goBack(BuildContext context) {
    Navigator.of(context).pop();
  }

  void pushScreen(BuildContext context, Widget screen) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => screen),
    );
  }

  void showToastSuccess(String? message) {
    Fluttertoast.showToast(
      msg: message ?? '',
      backgroundColor: Colors.green,
      fontSize: 16.0,
      gravity: ToastGravity.BOTTOM,
      textColor: Colors.white,
      timeInSecForIosWeb: 1,
      toastLength: Toast.LENGTH_LONG,
      webPosition: 'center',
      webBgColor: '#8bc34a',
    );
  }

  void showToastError(String? message) {
    Fluttertoast.showToast(
      msg: message ?? '',
      backgroundColor: Colors.red,
      fontSize: 16.0,
      gravity: ToastGravity.BOTTOM,
      textColor: Colors.white,
      timeInSecForIosWeb: 1,
      toastLength: Toast.LENGTH_LONG,
      webPosition: 'center',
      webBgColor: '#f44336',
    );
  }

  String? validateString(String? value) {
    if (value == null || value.trim() == '') {
      return 'El campo es requerido';
    }
  }

  String? validateDate(DateTime? value) {
    if (value == null) {
      return 'El campo es requerido';
    }
  }

  String? validateEmail(String? email) {
    final validation = validateString(email);

    if (validation != null) {
      return validation;
    }

    if (!RegExp(r'^[a-zA-Z0-9.a-zA-Z0-9._\-]+@[a-zA-Z0-9]+\.[a-zA-Z]+')
        .hasMatch(email!)) {
      return 'El correo no cumple con el formato';
    }
  }
}
