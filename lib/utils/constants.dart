// coverage:ignore-file
class Constants {
  // static const String urlBackend = 'http://192.168.0.44:3000/api'; // ** LOCAL
  static const String urlBackend =
      'http://18.205.157.200:3000/api'; // ** AWS EC2
  static const String contentTypeHeader = 'application/json';
  static const String authorizationHeader = 'Bearer';

  // User
  static const String clientUrl = '/clients/';
  // Vehicle
  static const String vehicleUrl = '/vehicles/';
  // Price
  static const String priceUrl = '/prices/';
  // Rental
  static const String rentalUrl = '/rentals/';
}
