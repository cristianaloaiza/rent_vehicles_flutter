import 'dart:convert';

import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart';
import 'package:rent_vehicles_flutter/utils/constants.dart';
import 'package:rent_vehicles_flutter/utils/helpers.dart';

class VehicleApiService {
  var vehicleHelper = ClientHelper(Constants.vehicleUrl);

  VehicleApiService();

  Future<ApiResponse> getAllVehicles() async {
    var apiResponse = await vehicleHelper.get();

    if (apiResponse.status == true) {
      var listVehicles = <Vehicle>[];
      var list = (apiResponse.data['vehicles'] ?? []) as List;

      list.forEach((vehicle) {
        listVehicles.add(Vehicle.fromJson(vehicle));
      });

      apiResponse.data = listVehicles;
    }

    return apiResponse;
  }

  Future<ApiResponse> getVehicleById(int vehicleId) async {
    var apiResponse = await vehicleHelper.get(param: vehicleId.toString());

    if (apiResponse.status == true) {
      apiResponse.data = Vehicle.fromJson(apiResponse.data['vehicle']);
    }

    return apiResponse;
  }

  Future<ApiResponse> storeVehicle(Vehicle vehicle) async {
    var body = jsonEncode(vehicle.toJson());

    var apiResponse = await vehicleHelper.post(body: body);

    if (apiResponse.status == true) {
      apiResponse.data = Vehicle.fromJson(apiResponse.data['vehicle']);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateVehicle(Vehicle vehicle) async {
    var body = jsonEncode(vehicle.toJson());

    var apiResponse =
        await vehicleHelper.put(param: vehicle.id.toString(), body: body);

    if (apiResponse.status == true) {
      apiResponse.data = vehicle;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteVehicle(int vehicleId) async {
    var apiResponse = await vehicleHelper.delete(param: vehicleId.toString());

    return apiResponse;
  }
}
