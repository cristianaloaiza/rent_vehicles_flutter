import 'dart:convert';

import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/rentals.dart';
import 'package:rent_vehicles_flutter/utils/constants.dart';
import 'package:rent_vehicles_flutter/utils/helpers.dart';

class RentalsApiService {
  var rentalsHelper = ClientHelper(Constants.rentalUrl);

  RentalsApiService();

  Future<ApiResponse> getAllRentals() async {
    var apiResponse = await rentalsHelper.get();

    if (apiResponse.status == true) {
      var listRentals = <Rentals>[];
      var list = (apiResponse.data['rentals'] ?? []) as List;

      list.forEach((rentals) {
        listRentals.add(Rentals.fromJson(rentals));
      });

      apiResponse.data = listRentals;
    }

    return apiResponse;
  }

  Future<ApiResponse> getRentalsById(int rentalsId) async {
    var apiResponse = await rentalsHelper.get(param: rentalsId.toString());

    if (apiResponse.status == true) {
      apiResponse.data = Rentals.fromJson(apiResponse.data['rentals']);
    }

    return apiResponse;
  }

  Future<ApiResponse> storeRentals(Rentals rentals) async {
    var body = jsonEncode(rentals.toJson());

    var apiResponse = await rentalsHelper.post(body: body);

    if (apiResponse.status == true) {
      apiResponse.data = Rentals.fromJson(apiResponse.data['rentals']);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateRentals(Rentals rentals) async {
    var body = jsonEncode(rentals.toJson());

    var apiResponse =
        await rentalsHelper.put(param: rentals.id.toString(), body: body);

    if (apiResponse.status == true) {
      apiResponse.data = rentals;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteRentals(int rentalsId) async {
    var apiResponse = await rentalsHelper.delete(param: rentalsId.toString());

    return apiResponse;
  }
}
