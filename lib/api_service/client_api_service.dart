import 'dart:convert';

import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/client.dart';
import 'package:rent_vehicles_flutter/utils/constants.dart';
import 'package:rent_vehicles_flutter/utils/helpers.dart';

class ClientApiService {
  var clientHelper = ClientHelper(Constants.clientUrl);

  ClientApiService();

  Future<ApiResponse> getAllClients() async {
    var apiResponse = await clientHelper.get();

    if (apiResponse.status == true) {
      var listClients = <Client>[];
      var list = (apiResponse.data['clients'] ?? []) as List;

      list.forEach((client) {
        listClients.add(Client.fromJson(client));
      });

      apiResponse.data = listClients;
    }

    return apiResponse;
  }

  Future<ApiResponse> getClientById(int clientId) async {
    var apiResponse = await clientHelper.get(param: clientId.toString());

    if (apiResponse.status == true) {
      apiResponse.data = Client.fromJson(apiResponse.data['client']);
    }

    return apiResponse;
  }

  Future<ApiResponse> storeClient(Client client) async {
    var body = jsonEncode(client.toJson());

    var apiResponse = await clientHelper.post(body: body);

    if (apiResponse.status == true) {
      apiResponse.data = Client.fromJson(apiResponse.data['client']);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateClient(Client client) async {
    var body = jsonEncode(client.toJson());

    var apiResponse =
        await clientHelper.put(param: client.id.toString(), body: body);

    if (apiResponse.status == true) {
      apiResponse.data = client;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteClient(int clientId) async {
    var apiResponse = await clientHelper.delete(param: clientId.toString());

    return apiResponse;
  }
}
