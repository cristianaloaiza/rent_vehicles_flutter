import 'dart:convert';

import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/price.dart';
import 'package:rent_vehicles_flutter/utils/constants.dart';
import 'package:rent_vehicles_flutter/utils/helpers.dart';

class PriceApiService {
  var priceHelper = ClientHelper(Constants.priceUrl);

  PriceApiService();

  Future<ApiResponse> getAllPrices() async {
    var apiResponse = await priceHelper.get();

    if (apiResponse.status == true) {
      var listPrices = <Price>[];
      var list = (apiResponse.data['prices'] ?? []) as List;

      list.forEach((price) {
        listPrices.add(Price.fromJson(price));
      });

      apiResponse.data = listPrices;
    }

    return apiResponse;
  }

  Future<ApiResponse> getPriceById(int priceId) async {
    var apiResponse = await priceHelper.get(param: priceId.toString());

    if (apiResponse.status == true) {
      apiResponse.data = Price.fromJson(apiResponse.data['price']);
    }

    return apiResponse;
  }

  Future<ApiResponse> storePrice(Price price) async {
    var body = jsonEncode(price.toJson());

    var apiResponse = await priceHelper.post(body: body);

    if (apiResponse.status == true) {
      apiResponse.data = Price.fromJson(apiResponse.data['price']);
    }

    return apiResponse;
  }

  Future<ApiResponse> updatePrice(Price price) async {
    var body = jsonEncode(price.toJson());

    var apiResponse =
        await priceHelper.put(param: price.id.toString(), body: body);

    if (apiResponse.status == true) {
      apiResponse.data = price;
    }

    return apiResponse;
  }

  Future<ApiResponse> deletePrice(int priceId) async {
    var apiResponse = await priceHelper.delete(param: priceId.toString());

    return apiResponse;
  }
}
