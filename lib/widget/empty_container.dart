import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmptyContainer extends StatelessWidget {
  EmptyContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 0,
      height: 0,
    );
  }
}
