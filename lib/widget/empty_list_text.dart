import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmptyListText extends StatelessWidget {
  final String text;

  EmptyListText({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(text,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w900,
          ),
          textAlign: TextAlign.center),
    );
  }
}
