import 'dart:async';

import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/rentals.dart';
import 'package:rent_vehicles_flutter/repository/rentals_repository.dart';

class RentalsBloc {
  var rentalsRepository = RentalsRepository();

  final _rentalsController = StreamController<Rentals>();
  final _rentalsListController = StreamController<List<Rentals>>();

  var _rentals = Rentals();
  var _rentalsList = <Rentals>[];

  Stream<List<Rentals>> get rentalsListStream => _rentalsListController.stream;

  Stream<Rentals> get rentalsStream => _rentalsController.stream;

  List<Rentals> getRentalsList() => _rentalsList;

  Rentals getRentals() => _rentals;

  void setRentalsList(List<Rentals> rentalsList) {
    _rentalsList = rentalsList;
  }

  void setRentals(Rentals rentals) {
    _rentals = rentals;
  }

  Future<ApiResponse> getAllRentals() async {
    var apiResponse = await rentalsRepository.getAllRentals();
    var list = <Rentals>[];

    if (apiResponse.status == true) {
      list = apiResponse.data as List<Rentals>;
    }

    _rentalsListController.add(list);
    _rentalsList = list;

    return apiResponse;
  }

  Future<ApiResponse> getRentalsById(int rentalsId) async {
    var apiResponse = await rentalsRepository.getRentalsById(rentalsId);

    var rentals = Rentals();

    if (apiResponse.status == true) {
      rentals = apiResponse.data as Rentals;
    }

    _rentalsController.add(rentals);
    _rentals = rentals;

    return apiResponse;
  }

  Future<ApiResponse> storeRentals(Rentals rentals) async {
    var apiResponse = await rentalsRepository.storeRentals(rentals);

    if (apiResponse.status == true) {
      _rentals = apiResponse.data as Rentals;
      _rentalsList.add(_rentals);

      _rentalsController.add(_rentals);
      _rentalsListController.add(_rentalsList);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateRentals(Rentals rentals) async {
    var apiResponse = await rentalsRepository.updateRentals(rentals);

    if (apiResponse.status == true) {
      _rentals = rentals;
      _rentalsController.add(_rentals);

      var index = _rentalsList.indexWhere((e) => e.id == _rentals.id);

      if (index == -1) {
        _rentalsList.add(_rentals);
      } else {
        _rentalsList[index] = _rentals;
      }

      _rentalsListController.add(_rentalsList);
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteRentals(int rentalsId) async {
    var apiResponse = await rentalsRepository.deleteRentals(rentalsId);

    if (apiResponse.status == true) {
      var index = _rentalsList.indexWhere((e) => e.id == _rentals.id);

      if (index >= 0) {
        _rentalsList.removeAt(index);
        _rentalsListController.add(_rentalsList);
      }
    }

    return apiResponse;
  }

  void dispose() {
    _rentalsListController.close();
    _rentalsController.close();
  }
}
