import 'dart:async';

import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/client.dart';
import 'package:rent_vehicles_flutter/repository/client_repository.dart';

class ClientBloc {
  var clientRepository = ClientRepository();

  final _clientController = StreamController<Client>();
  final _clientListController = StreamController<List<Client>>();

  var _client = Client();
  var _clientList = <Client>[];

  Stream<List<Client>> get clientListStream => _clientListController.stream;

  Stream<Client> get clientStream => _clientController.stream;

  List<Client> getClientList() => _clientList;

  Client getClient() => _client;

  void setClientList(List<Client> clientList) {
    _clientList = clientList;
  }

  void setClient(Client client) {
    _client = client;
  }

  Future<ApiResponse> getAllClients() async {
    var apiResponse = await clientRepository.getAllClients();
    var list = <Client>[];

    if (apiResponse.status == true) {
      list = apiResponse.data as List<Client>;
    }

    _clientListController.add(list);
    _clientList = list;

    return apiResponse;
  }

  Future<ApiResponse> getClientById(int clientId) async {
    var apiResponse = await clientRepository.getClientById(clientId);

    var client = Client();

    if (apiResponse.status == true) {
      client = apiResponse.data as Client;
    }

    _clientController.add(client);
    _client = client;

    return apiResponse;
  }

  Future<ApiResponse> storeClient(Client client) async {
    var apiResponse = await clientRepository.storeClient(client);

    if (apiResponse.status == true) {
      _client = apiResponse.data as Client;
      _clientList.add(_client);

      _clientController.add(_client);
      _clientListController.add(_clientList);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateClient(Client client) async {
    var apiResponse = await clientRepository.updateClient(client);

    if (apiResponse.status == true) {
      _client = client;
      _clientController.add(_client);

      var index = _clientList.indexWhere((e) => e.id == _client.id);

      if (index == -1) {
        _clientList.add(_client);
      } else {
        _clientList[index] = _client;
      }

      _clientListController.add(_clientList);
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteClient(int clientId) async {
    var apiResponse = await clientRepository.deleteClient(clientId);

    if (apiResponse.status == true) {
      var index = _clientList.indexWhere((e) => e.id == _client.id);

      if (index >= 0) {
        _clientList.removeAt(index);
        _clientListController.add(_clientList);
      }
    }

    return apiResponse;
  }

  void dispose() {
    _clientListController.close();
    _clientController.close();
  }
}
