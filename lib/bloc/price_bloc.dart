import 'dart:async';

import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/price.dart';
import 'package:rent_vehicles_flutter/repository/price_repository.dart';

class PriceBloc {
  var priceRepository = PriceRepository();

  final _priceController = StreamController<Price>();
  final _priceListController = StreamController<List<Price>>();

  var _price = Price();
  var _priceList = <Price>[];

  Stream<List<Price>> get priceListStream => _priceListController.stream;

  Stream<Price> get priceStream => _priceController.stream;

  List<Price> getPriceList() => _priceList;

  Price getPrice() => _price;

  void setPriceList(List<Price> priceList) {
    _priceList = priceList;
  }

  void setPrice(Price price) {
    _price = price;
  }

  Future<ApiResponse> getAllPrices() async {
    var apiResponse = await priceRepository.getAllPrices();
    var list = <Price>[];

    if (apiResponse.status == true) {
      list = apiResponse.data as List<Price>;
    }

    _priceListController.add(list);
    _priceList = list;

    return apiResponse;
  }

  Future<ApiResponse> getPriceById(int priceId) async {
    var apiResponse = await priceRepository.getPriceById(priceId);

    var price = Price();

    if (apiResponse.status == true) {
      price = apiResponse.data as Price;
    }

    _priceController.add(price);
    _price = price;

    return apiResponse;
  }

  Future<ApiResponse> storePrice(Price price) async {
    var apiResponse = await priceRepository.storePrice(price);

    if (apiResponse.status == true) {
      _price = apiResponse.data as Price;
      _priceList.add(_price);

      _priceController.add(_price);
      _priceListController.add(_priceList);
    }

    return apiResponse;
  }

  Future<ApiResponse> updatePrice(Price price) async {
    var apiResponse = await priceRepository.updatePrice(price);

    if (apiResponse.status == true) {
      _price = price;
      _priceController.add(_price);

      var index = _priceList.indexWhere((e) => e.id == _price.id);

      if (index == -1) {
        _priceList.add(_price);
      } else {
        _priceList[index] = _price;
      }

      _priceListController.add(_priceList);
    }

    return apiResponse;
  }

  Future<ApiResponse> deletePrice(int priceId) async {
    var apiResponse = await priceRepository.deletePrice(priceId);

    if (apiResponse.status == true) {
      var index = _priceList.indexWhere((e) => e.id == _price.id);

      if (index >= 0) {
        _priceList.removeAt(index);
        _priceListController.add(_priceList);
      }
    }

    return apiResponse;
  }

  void dispose() {
    _priceListController.close();
    _priceController.close();
  }
}
