import 'dart:async';

import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart';
import 'package:rent_vehicles_flutter/repository/vehicle_repository.dart';

class VehicleBloc {
  var vehicleRepository = VehicleRepository();

  final _vehicleController = StreamController<Vehicle>();
  final _vehicleListController = StreamController<List<Vehicle>>();

  var _vehicle = Vehicle();
  var _vehicleList = <Vehicle>[];

  Stream<List<Vehicle>> get vehicleListStream => _vehicleListController.stream;

  Stream<Vehicle> get vehicleStream => _vehicleController.stream;

  List<Vehicle> getVehicleList() => _vehicleList;

  Vehicle getVehicle() => _vehicle;

  void setVehicleList(List<Vehicle> vehicleList) {
    _vehicleList = vehicleList;
  }

  void setVehicle(Vehicle vehicle) {
    _vehicle = vehicle;
  }

  Future<ApiResponse> getAllVehicles() async {
    var apiResponse = await vehicleRepository.getAllVehicles();
    var list = <Vehicle>[];

    if (apiResponse.status == true) {
      list = apiResponse.data as List<Vehicle>;
    }

    _vehicleListController.add(list);
    _vehicleList = list;

    return apiResponse;
  }

  Future<ApiResponse> getVehicleById(int vehicleId) async {
    var apiResponse = await vehicleRepository.getVehicleById(vehicleId);

    var vehicle = Vehicle();

    if (apiResponse.status == true) {
      vehicle = apiResponse.data as Vehicle;
    }

    _vehicleController.add(vehicle);
    _vehicle = vehicle;

    return apiResponse;
  }

  Future<ApiResponse> storeVehicle(Vehicle vehicle) async {
    var apiResponse = await vehicleRepository.storeVehicle(vehicle);

    if (apiResponse.status == true) {
      _vehicle = apiResponse.data as Vehicle;
      _vehicleList.add(_vehicle);

      _vehicleController.add(_vehicle);
      _vehicleListController.add(_vehicleList);
    }

    return apiResponse;
  }

  Future<ApiResponse> updateVehicle(Vehicle vehicle) async {
    var apiResponse = await vehicleRepository.updateVehicle(vehicle);

    if (apiResponse.status == true) {
      _vehicle = vehicle;
      _vehicleController.add(_vehicle);

      var index = _vehicleList.indexWhere((e) => e.id == _vehicle.id);

      if (index == -1) {
        _vehicleList.add(_vehicle);
      } else {
        _vehicleList[index] = _vehicle;
      }

      _vehicleListController.add(_vehicleList);
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteVehicle(int vehicleId) async {
    var apiResponse = await vehicleRepository.deleteVehicle(vehicleId);

    if (apiResponse.status == true) {
      var index = _vehicleList.indexWhere((e) => e.id == _vehicle.id);

      if (index >= 0) {
        _vehicleList.removeAt(index);
        _vehicleListController.add(_vehicleList);
      }
    }

    return apiResponse;
  }

  void dispose() {
    _vehicleListController.close();
    _vehicleController.close();
  }
}
