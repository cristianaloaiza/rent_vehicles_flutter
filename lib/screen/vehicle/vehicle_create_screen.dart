import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/bloc/vehicle_bloc.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart';
import 'package:rent_vehicles_flutter/screen/vehicle/vehicle_const.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';

class VehicleCreateScreen extends StatefulWidget {
  final VehicleBloc vehicleBloc;

  VehicleCreateScreen({Key? key, required this.vehicleBloc}) : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _VehicleScreen(vehicleBloc: vehicleBloc);
}

class _VehicleScreen extends State<VehicleCreateScreen> {
  final _formKey = GlobalKey<FormState>();
  final _vehicle = Vehicle(
      status: VehicleConsts.VEHICLE_STATUS[VehicleStatusType.ACTIVE]?.value);
  final _utils = Utils();
  final VehicleBloc vehicleBloc;
  var _loading = false;

  _VehicleScreen({required this.vehicleBloc});

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void _onCreateVehicle() {
      if (_formKey.currentState?.validate() == true) {
        setState(() {
          _loading = true;
        });

        vehicleBloc.storeVehicle(_vehicle).then((response) {
          if (response.status == true) {
            _utils.showToastSuccess(response.message);
            _utils.goBack(context);
          } else {
            _utils.showToastError(response.message);
          }

          setState(() {
            _loading = false;
          });
        });
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Crear vehículo'),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(height: 10),
                  DropdownButtonFormField<String>(
                    autofocus: true,
                    decoration: InputDecoration(labelText: 'Tipo'),
                    value: _vehicle.type,
                    onChanged: (String? value) {
                      setState(() {
                        _vehicle.type = value!;
                      });
                    },
                    validator: _utils.validateString,
                    items: VehicleConsts.VEHICLE_TYPE_LIST
                        .map<DropdownMenuItem<String>>(
                      (typ) {
                        return DropdownMenuItem<String>(
                          value: typ.value,
                          child: Text(typ.label),
                        );
                      },
                    ).toList(),
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Modelo'),
                    onChanged: (value) => _vehicle.model = value,
                    validator: _utils.validateString,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Cilindraje'),
                    onChanged: (value) => _vehicle.cylinder_capacity = value,
                    validator: _utils.validateString,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Color'),
                    onChanged: (value) => _vehicle.colour = value,
                    validator: _utils.validateString,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Placa'),
                    onChanged: (value) => _vehicle.plate = value,
                    validator: _utils.validateString,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Kilometraje'),
                    onChanged: (value) => _vehicle.mileage = value,
                    validator: _utils.validateString,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Observaciones'),
                    onChanged: (value) => _vehicle.observations = value,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _loading ? null : _onCreateVehicle,
        tooltip: 'Crear vehículo',
        backgroundColor: Colors.green,
        child: _loading
            ? CircularProgressIndicator(
                color: Colors.white,
              )
            : Icon(Icons.save_outlined),
      ),
    );
  }
}
