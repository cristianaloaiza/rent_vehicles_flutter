import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/bloc/vehicle_bloc.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart';
import 'package:rent_vehicles_flutter/screen/vehicle/vehicle_const.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';

class VehicleEditScreen extends StatefulWidget {
  final VehicleBloc vehicleBloc;
  final int vehicleId;

  VehicleEditScreen(
      {Key? key, required this.vehicleBloc, required this.vehicleId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _VehicleScreen(vehicleBloc: vehicleBloc, vehicleId: vehicleId);
}

class _VehicleScreen extends State<VehicleEditScreen> {
  final _formKey = GlobalKey<FormState>();
  final _utils = Utils();
  final int vehicleId;
  final VehicleBloc vehicleBloc;
  late Vehicle _vehicle;
  var _loading = false;
  var _skeleton = true;

  _VehicleScreen({required this.vehicleBloc, required this.vehicleId}) {
    vehicleBloc.getVehicleById(vehicleId).then((response) {
      if (response.status == true) {
        _vehicle = response.data;

        setState(() {
          _skeleton = false;
        });
      } else {
        _utils.showToastError(response.message);
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void _onDeleteVehicle() {
      setState(() {
        _loading = true;
      });

      vehicleBloc.deleteVehicle(vehicleId).then((response) {
        if (response.status == true) {
          _utils.showToastSuccess(response.message);
          _utils.goBack(context);
        } else {
          _utils.showToastError(response.message);
        }

        setState(() {
          _loading = false;
        });
      });
    }

    void _onUpdateVehicle() {
      if (_formKey.currentState?.validate() == true) {
        setState(() {
          _loading = true;
        });

        vehicleBloc.updateVehicle(_vehicle).then((response) {
          if (response.status == true) {
            _utils.showToastSuccess(response.message);
          } else {
            _utils.showToastError(response.message);
          }

          setState(() {
            _loading = false;
          });
        });
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: const Text('Editar vehículo'),
        ),
        body: _skeleton == true
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          SizedBox(height: 10),
                          DropdownButtonFormField<String>(
                            autofocus: true,
                            decoration: InputDecoration(labelText: 'Tipo'),
                            value: _vehicle.type,
                            onChanged: (String? value) {
                              setState(() {
                                _vehicle.type = value!;
                              });
                            },
                            validator: _utils.validateString,
                            items: VehicleConsts.VEHICLE_TYPE_LIST
                                .map<DropdownMenuItem<String>>(
                              (typ) {
                                return DropdownMenuItem<String>(
                                  value: typ.value,
                                  child: Text(typ.label),
                                );
                              },
                            ).toList(),
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration: InputDecoration(labelText: 'Modelo'),
                            initialValue: _vehicle.model,
                            onChanged: (value) => _vehicle.model = value,
                            validator: _utils.validateString,
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Cilindraje'),
                            initialValue: _vehicle.cylinder_capacity,
                            onChanged: (value) =>
                                _vehicle.cylinder_capacity = value,
                            validator: _utils.validateString,
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration: InputDecoration(labelText: 'Color'),
                            initialValue: _vehicle.colour,
                            onChanged: (value) => _vehicle.colour = value,
                            validator: _utils.validateString,
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration: InputDecoration(labelText: 'Placa'),
                            initialValue: _vehicle.plate,
                            onChanged: (value) => _vehicle.plate = value,
                            validator: _utils.validateString,
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Kilometraje'),
                            initialValue: _vehicle.mileage,
                            onChanged: (value) => _vehicle.mileage = value,
                            validator: _utils.validateString,
                          ),
                          SizedBox(height: 10),
                          DropdownButtonFormField<String>(
                            decoration: InputDecoration(labelText: 'Estado'),
                            value: _vehicle.status,
                            onChanged: (String? value) {
                              setState(() {
                                _vehicle.status = value!;
                              });
                            },
                            validator: _utils.validateString,
                            items: VehicleConsts.VEHICLE_STATUS_LIST
                                .map<DropdownMenuItem<String>>(
                              (status) {
                                return DropdownMenuItem<String>(
                                  value: status.value,
                                  child: Text(status.label),
                                );
                              },
                            ).toList(),
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Observaciones'),
                            initialValue: _vehicle.observations,
                            onChanged: (value) => _vehicle.observations = value,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
        floatingActionButton: _skeleton == true
            ? null
            : Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: FloatingActionButton(
                      onPressed: _loading
                          ? null
                          : () => showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  title: const Text('Eliminar vehículo'),
                                  content: const Text(
                                      '¿Confirma eliminar el vehículo?'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(),
                                      child: const Text('Cancelar'),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                        _onDeleteVehicle();
                                      },
                                      child: const Text('Aceptar'),
                                    ),
                                  ],
                                ),
                              ),
                      tooltip: 'Eliminar vehículo',
                      backgroundColor: Colors.red,
                      child: _loading
                          ? CircularProgressIndicator(
                              color: Colors.white,
                            )
                          : Icon(Icons.delete),
                    ),
                  ),
                  FloatingActionButton(
                    onPressed: _loading ? null : _onUpdateVehicle,
                    tooltip: 'Actualizar vehículo',
                    backgroundColor: Colors.blue,
                    child: _loading
                        ? CircularProgressIndicator(
                            color: Colors.white,
                          )
                        : Icon(Icons.sync),
                  ),
                ],
              ));
  }
}
