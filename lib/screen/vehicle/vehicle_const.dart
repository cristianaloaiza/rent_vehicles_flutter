// coverage:ignore-file
import 'package:rent_vehicles_flutter/utils/utils.dart';

enum VehicleType { CARRO, MOTO }
enum VehicleStatusType { ACTIVE, INACTIVE }

class VehicleConsts {
  static final Map<VehicleType, ListItem> VEHICLE_TYPE = {
    VehicleType.CARRO: ListItem(value: 'CARRO', label: 'CARRO'),
    VehicleType.MOTO: ListItem(value: 'MOTO', label: 'MOTO'),
  };

  static final List<ListItem> VEHICLE_TYPE_LIST = [
    VEHICLE_TYPE[VehicleType.CARRO]!,
    VEHICLE_TYPE[VehicleType.MOTO]!,
  ];

  static final Map<VehicleStatusType, ListItem> VEHICLE_STATUS = {
    VehicleStatusType.ACTIVE: ListItem(value: 'ACTIVE', label: 'Activo'),
    VehicleStatusType.INACTIVE: ListItem(value: 'INACTIVE', label: 'Inactivo'),
  };

  static final List<ListItem> VEHICLE_STATUS_LIST = [
    VehicleConsts.VEHICLE_STATUS[VehicleStatusType.ACTIVE]!,
    VehicleConsts.VEHICLE_STATUS[VehicleStatusType.INACTIVE]!
  ];
}
