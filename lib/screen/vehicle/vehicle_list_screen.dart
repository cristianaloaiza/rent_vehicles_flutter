import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/bloc/vehicle_bloc.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart';
import 'package:rent_vehicles_flutter/screen/vehicle/vehicle_const.dart';
import 'package:rent_vehicles_flutter/screen/vehicle/vehicle_create_screen.dart';
import 'package:rent_vehicles_flutter/screen/vehicle/vehicle_edit_screen.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';
import 'package:rent_vehicles_flutter/widget/empty_list_text.dart';

class VehicleListScreen extends StatefulWidget {
  const VehicleListScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _VehicleScreen();
}

class _VehicleScreen extends State<VehicleListScreen> {
  final _vehicleBloc = VehicleBloc();
  final _utils = Utils();
  var _skeleton = true;

  @override
  void initState() {
    super.initState();

    _vehicleBloc.getAllVehicles().then((response) {
      if (response.status == true) {
        setState(() {
          _skeleton = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    VehicleType _getRole(str) => VehicleType.values
        .firstWhere((e) => e.toString() == 'VehicleType.' + str);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Listado de vehículos'),
      ),
      body: _skeleton == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : StreamBuilder(
              stream: _vehicleBloc.vehicleListStream,
              builder: (
                BuildContext context,
                AsyncSnapshot<List<Vehicle>> snapshot,
              ) {
                final _vehicleList = snapshot.data ?? [];

                if (_vehicleList.isEmpty) {
                  return EmptyListText(text: 'Listado de vehículos vacío');
                }

                return ListView.builder(
                    itemCount: _vehicleList.length,
                    itemBuilder: (BuildContext context, int index) {
                      final vehicle = _vehicleList[index];
                      final _isActive =
                          ('VehicleStatusType.' + vehicle.status!) ==
                              VehicleStatusType.ACTIVE.toString();

                      return ListTile(
                        leading: Icon(
                          Icons.car_rental,
                          color: _isActive == true ? Colors.green : Colors.red,
                        ),
                        title: Text(VehicleConsts
                                .VEHICLE_TYPE[_getRole(vehicle.type)]?.label ??
                            ''),
                        subtitle: Text(vehicle.plate!),
                        trailing: Chip(
                          label: Text(vehicle.colour!),
                          backgroundColor: Colors.blue[800],
                          labelStyle: const TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onTap: () => _utils.pushScreen(
                            context,
                            VehicleEditScreen(
                              vehicleBloc: _vehicleBloc,
                              vehicleId: vehicle.id!,
                            )),
                      );
                    });
              }),
      floatingActionButton: _skeleton
          ? null
          : FloatingActionButton(
              onPressed: () => _utils.pushScreen(
                  context,
                  VehicleCreateScreen(
                    vehicleBloc: _vehicleBloc,
                  )),
              tooltip: 'Crear vehículo',
              backgroundColor: Colors.green,
              child: const Icon(Icons.add),
            ),
    );
  }
}
