// coverage:ignore-file
import 'package:flutter/services.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart';
import 'package:rent_vehicles_flutter/screen/vehicle/vehicle_const.dart';

class VehicleFormValidator {
  FilteringTextInputFormatter formatDocumentNumber() =>
      FilteringTextInputFormatter.allow(RegExp(r'^(\d+)'));

  static List<Vehicle> getVehicleListActive(List<Vehicle>? list) =>
      list
          ?.where((vehicle) =>
              'VehicleStatusType.' + vehicle.status! ==
              VehicleStatusType.ACTIVE.toString())
          .toList() ??
      [];
}
