import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/bloc/client_bloc.dart';
import 'package:rent_vehicles_flutter/model/client.dart';
import 'package:rent_vehicles_flutter/screen/client/client_const.dart';
import 'package:rent_vehicles_flutter/screen/client/client_create_screen.dart';
import 'package:rent_vehicles_flutter/screen/client/client_edit_screen.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';
import 'package:rent_vehicles_flutter/widget/empty_list_text.dart';

class ClientListScreen extends StatefulWidget {
  const ClientListScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ClientScreen();
}

class _ClientScreen extends State<ClientListScreen> {
  final _clientBloc = ClientBloc();
  final _utils = Utils();
  var _skeleton = true;

  @override
  void initState() {
    super.initState();

    _clientBloc.getAllClients().then((response) {
      if (response.status == true) {
        setState(() {
          _skeleton = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    ClientLicenseType _getRole(str) => ClientLicenseType.values
        .firstWhere((e) => e.toString() == 'ClientLicenseType.' + str);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Listado de clientes'),
      ),
      body: _skeleton == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : StreamBuilder(
              stream: _clientBloc.clientListStream,
              builder: (
                BuildContext context,
                AsyncSnapshot<List<Client>> snapshot,
              ) {
                final _clientList = snapshot.data ?? [];

                if (_clientList.isEmpty) {
                  return EmptyListText(text: 'Listado de clientes vacío');
                }

                return ListView.builder(
                    itemCount: _clientList.length,
                    itemBuilder: (BuildContext context, int index) {
                      final client = _clientList[index];
                      final _isActive =
                          ('ClientStatusType.' + client.status!) ==
                              ClientStatusType.ACTIVE.toString();

                      return ListTile(
                        leading: Icon(
                          Icons.person,
                          color: _isActive == true ? Colors.green : Colors.red,
                        ),
                        title: Text(client.name!),
                        subtitle: Text(client.email!),
                        trailing: Chip(
                          label: Text(ClientConsts
                                  .CLIENT_LICENSE[_getRole(client.license)]
                                  ?.label ??
                              ''),
                          backgroundColor: Colors.blue[800],
                          labelStyle: const TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onTap: () => _utils.pushScreen(
                            context,
                            ClientEditScreen(
                              clientBloc: _clientBloc,
                              clientId: client.id!,
                            )),
                      );
                    });
              }),
      floatingActionButton: _skeleton
          ? null
          : FloatingActionButton(
              onPressed: () => _utils.pushScreen(
                  context,
                  ClientCreateScreen(
                    clientBloc: _clientBloc,
                  )),
              tooltip: 'Crear cliente',
              backgroundColor: Colors.green,
              child: const Icon(Icons.add),
            ),
    );
  }
}
