import 'package:date_field/date_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/bloc/client_bloc.dart';
import 'package:rent_vehicles_flutter/model/client.dart';
import 'package:rent_vehicles_flutter/screen/client/client_const.dart';
import 'package:rent_vehicles_flutter/screen/client/client_form_validator.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';

class ClientEditScreen extends StatefulWidget {
  final ClientBloc clientBloc;
  final int clientId;

  ClientEditScreen({Key? key, required this.clientBloc, required this.clientId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _ClientScreen(clientBloc: clientBloc, clientId: clientId);
}

class _ClientScreen extends State<ClientEditScreen> {
  final _formKey = GlobalKey<FormState>();
  final _clientFormValidator = ClientFormValidator();
  final _utils = Utils();
  final int clientId;
  final ClientBloc clientBloc;
  late Client _client;
  var _loading = false;
  var _skeleton = true;

  _ClientScreen({required this.clientBloc, required this.clientId}) {
    clientBloc.getClientById(clientId).then((response) {
      if (response.status == true) {
        _client = response.data;

        setState(() {
          _skeleton = false;
        });
      } else {
        _utils.showToastError(response.message);
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void _onDeleteClient() {
      setState(() {
        _loading = true;
      });

      clientBloc.deleteClient(_client.id!).then((response) {
        if (response.status == true) {
          _utils.showToastSuccess(response.message);
          _utils.goBack(context);
        } else {
          _utils.showToastError(response.message);
        }

        setState(() {
          _loading = false;
        });
      });
    }

    void _onUpdateClient() {
      if (_formKey.currentState?.validate() == true) {
        setState(() {
          _loading = true;
        });

        clientBloc.updateClient(_client).then((response) {
          if (response.status == true) {
            _utils.showToastSuccess(response.message);
          } else {
            _utils.showToastError(response.message);
          }

          setState(() {
            _loading = false;
          });
        });
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: const Text('Editar cliente'),
        ),
        body: _skeleton == true
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          SizedBox(height: 10),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'Número de documento'),
                            initialValue: _client.document_number,
                            onChanged: (value) =>
                                _client.document_number = value,
                            validator: _utils.validateString,
                            inputFormatters: [
                              _clientFormValidator.formatDocumentNumber()
                            ],
                            autofocus: true,
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration: InputDecoration(labelText: 'Nombre'),
                            initialValue: _client.name,
                            onChanged: (value) => _client.name = value,
                            validator: _utils.validateString,
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(labelText: 'Correo'),
                            initialValue: _client.email,
                            onChanged: (value) => _client.email = value,
                            validator: _utils.validateEmail,
                          ),
                          SizedBox(height: 10),
                          DateTimeFormField(
                            initialValue: DateTime.parse(_client.birthdate!),
                            decoration: const InputDecoration(
                              hintStyle: TextStyle(color: Colors.black45),
                              errorStyle: TextStyle(color: Colors.redAccent),
                              suffixIcon: Icon(Icons.event_note),
                              labelText: 'Fecha de nacimiento',
                            ),
                            validator: _utils.validateDate,
                            mode: DateTimeFieldPickerMode.date,
                            onDateSelected: (value) => _client.birthdate =
                                value.toIso8601String().toString(),
                          ),
                          SizedBox(height: 10),
                          DropdownButtonFormField<String>(
                            decoration: InputDecoration(labelText: 'Licencia'),
                            value: _client.license,
                            onChanged: (String? value) {
                              setState(() {
                                _client.license = value!;
                              });
                            },
                            validator: _utils.validateString,
                            items: ClientConsts.CLIENT_LICENSE_LIST
                                .map<DropdownMenuItem<String>>(
                              (license) {
                                return DropdownMenuItem<String>(
                                  value: license.value,
                                  child: Text(license.label),
                                );
                              },
                            ).toList(),
                          ),
                          SizedBox(height: 10),
                          DropdownButtonFormField<String>(
                            decoration: InputDecoration(labelText: 'Estado'),
                            value: _client.status,
                            onChanged: (String? value) {
                              setState(() {
                                _client.status = value!;
                              });
                            },
                            validator: _utils.validateString,
                            items: ClientConsts.CLIENT_STATUS_LIST
                                .map<DropdownMenuItem<String>>(
                              (status) {
                                return DropdownMenuItem<String>(
                                  value: status.value,
                                  child: Text(status.label),
                                );
                              },
                            ).toList(),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
        floatingActionButton: _skeleton == true
            ? null
            : Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: FloatingActionButton(
                      onPressed: _loading
                          ? null
                          : () => showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  title: const Text('Eliminar cliente'),
                                  content: const Text(
                                      '¿Confirma eliminar el cliente?'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(),
                                      child: const Text('Cancelar'),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                        _onDeleteClient();
                                      },
                                      child: const Text('Aceptar'),
                                    ),
                                  ],
                                ),
                              ),
                      tooltip: 'Eliminar cliente',
                      backgroundColor: Colors.red,
                      child: _loading
                          ? CircularProgressIndicator(
                              color: Colors.white,
                            )
                          : Icon(Icons.delete),
                    ),
                  ),
                  FloatingActionButton(
                    onPressed: _loading ? null : _onUpdateClient,
                    tooltip: 'Actualizar cliente',
                    backgroundColor: Colors.blue,
                    child: _loading
                        ? CircularProgressIndicator(
                            color: Colors.white,
                          )
                        : Icon(Icons.sync),
                  ),
                ],
              ));
  }
}
