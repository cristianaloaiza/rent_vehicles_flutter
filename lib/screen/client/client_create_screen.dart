import 'package:date_field/date_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rent_vehicles_flutter/bloc/client_bloc.dart';
import 'package:rent_vehicles_flutter/model/client.dart';
import 'package:rent_vehicles_flutter/screen/client/client_const.dart';
import 'package:rent_vehicles_flutter/screen/client/client_form_validator.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';

class ClientCreateScreen extends StatefulWidget {
  final ClientBloc clientBloc;

  ClientCreateScreen({Key? key, required this.clientBloc}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ClientScreen(clientBloc: clientBloc);
}

class _ClientScreen extends State<ClientCreateScreen> {
  final _formKey = GlobalKey<FormState>();
  final _client = Client(
      status: ClientConsts.CLIENT_STATUS[ClientStatusType.ACTIVE]?.value);
  final _clientFormValidator = ClientFormValidator();
  final _utils = Utils();
  final ClientBloc clientBloc;
  var _loading = false;

  _ClientScreen({required this.clientBloc});

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void _onCreateClient() {
      if (_formKey.currentState?.validate() == true) {
        setState(() {
          _loading = true;
        });

        clientBloc.storeClient(_client).then((response) {
          if (response.status == true) {
            _utils.showToastSuccess(response.message);
            _utils.goBack(context);
          } else {
            _utils.showToastError(response.message);
          }

          setState(() {
            _loading = false;
          });
        });
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Crear cliente'),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(height: 10),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration:
                        InputDecoration(labelText: 'Número de documento'),
                    onChanged: (value) => _client.document_number = value,
                    validator: _utils.validateString,
                    inputFormatters: [
                      _clientFormValidator.formatDocumentNumber()
                    ],
                    autofocus: true,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Nombre'),
                    onChanged: (value) => _client.name = value,
                    validator: _utils.validateString,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(labelText: 'Correo'),
                    onChanged: (value) => _client.email = value,
                    validator: _utils.validateEmail,
                  ),
                  SizedBox(height: 10),
                  DateTimeFormField(
                    decoration: const InputDecoration(
                      hintStyle: TextStyle(color: Colors.black45),
                      errorStyle: TextStyle(color: Colors.redAccent),
                      suffixIcon: Icon(Icons.event_note),
                      labelText: 'Fecha de nacimiento',
                    ),
                    validator: _utils.validateDate,
                    mode: DateTimeFieldPickerMode.date,
                    onDateSelected: (value) =>
                        _client.birthdate = value.toIso8601String().toString(),
                  ),
                  SizedBox(height: 10),
                  DropdownButtonFormField<String>(
                    decoration: InputDecoration(labelText: 'Licencia'),
                    value: _client.license,
                    onChanged: (String? value) {
                      setState(() {
                        _client.license = value!;
                      });
                    },
                    validator: _utils.validateString,
                    items: ClientConsts.CLIENT_LICENSE_LIST
                        .map<DropdownMenuItem<String>>(
                      (license) {
                        return DropdownMenuItem<String>(
                          value: license.value,
                          child: Text(license.label),
                        );
                      },
                    ).toList(),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _loading ? null : _onCreateClient,
        tooltip: 'Crear cliente',
        backgroundColor: Colors.green,
        child: _loading
            ? CircularProgressIndicator(
                color: Colors.white,
              )
            : Icon(Icons.save_outlined),
      ),
    );
  }
}
