// coverage:ignore-file
import 'package:flutter/services.dart';
import 'package:rent_vehicles_flutter/model/client.dart';
import 'package:rent_vehicles_flutter/screen/client/client_const.dart';

class ClientFormValidator {
  FilteringTextInputFormatter formatDocumentNumber() =>
      FilteringTextInputFormatter.allow(RegExp(r'^(\d+)'));

  static List<Client> getClientListActive(List<Client>? list) =>
      list
          ?.where((client) =>
              'ClientStatusType.' + client.status! ==
              ClientStatusType.ACTIVE.toString())
          .toList() ??
      [];
}
