// coverage:ignore-file
import 'package:rent_vehicles_flutter/utils/utils.dart';

enum ClientLicenseType { A1, A2, B1, B2, B3, C1, C2, C3 }
enum ClientStatusType { ACTIVE, INACTIVE }

class ClientConsts {
  static final Map<ClientLicenseType, ListItem> CLIENT_LICENSE = {
    ClientLicenseType.A1: ListItem(value: 'A1', label: 'A1'),
    ClientLicenseType.A2: ListItem(value: 'A2', label: 'A2'),
    ClientLicenseType.B1: ListItem(value: 'B1', label: 'B1'),
    ClientLicenseType.B2: ListItem(value: 'B2', label: 'B2'),
    ClientLicenseType.B3: ListItem(value: 'B3', label: 'B3'),
    ClientLicenseType.C1: ListItem(value: 'C1', label: 'C1'),
    ClientLicenseType.C2: ListItem(value: 'C2', label: 'C2'),
    ClientLicenseType.C3: ListItem(value: 'C3', label: 'C3'),
  };

  static final List<ListItem> CLIENT_LICENSE_LIST = [
    CLIENT_LICENSE[ClientLicenseType.A1]!,
    CLIENT_LICENSE[ClientLicenseType.A2]!,
    CLIENT_LICENSE[ClientLicenseType.B1]!,
    CLIENT_LICENSE[ClientLicenseType.B2]!,
    CLIENT_LICENSE[ClientLicenseType.B3]!,
    CLIENT_LICENSE[ClientLicenseType.C1]!,
    CLIENT_LICENSE[ClientLicenseType.C2]!,
    CLIENT_LICENSE[ClientLicenseType.C3]!,
  ];

  static final Map<ClientStatusType, ListItem> CLIENT_STATUS = {
    ClientStatusType.ACTIVE: ListItem(value: 'ACTIVE', label: 'Activo'),
    ClientStatusType.INACTIVE: ListItem(value: 'INACTIVE', label: 'Inactivo'),
  };

  static final List<ListItem> CLIENT_STATUS_LIST = [
    ClientConsts.CLIENT_STATUS[ClientStatusType.ACTIVE]!,
    ClientConsts.CLIENT_STATUS[ClientStatusType.INACTIVE]!
  ];
}
