// coverage:ignore-file
import 'package:flutter/services.dart';
import 'package:rent_vehicles_flutter/model/price.dart';
import 'package:rent_vehicles_flutter/screen/price/price_const.dart';

class PriceFormValidator {
  FilteringTextInputFormatter formatNumber() =>
      FilteringTextInputFormatter.allow(RegExp(r'^(\d+)'));

  static List<Price> getPriceListActive(List<Price>? list) =>
      list
          ?.where((price) =>
              'PriceStatusType.' + price.status! ==
              PriceStatusType.ACTIVE.toString())
          .toList() ??
      [];
}
