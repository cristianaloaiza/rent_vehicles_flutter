// coverage:ignore-file
import 'package:rent_vehicles_flutter/utils/utils.dart';

enum PriceType { CARRO, MOTO }
enum PriceStatusType { ACTIVE, INACTIVE }

class PriceConsts {
  static final Map<PriceType, ListItem> PRICE_TYPE = {
    PriceType.CARRO: ListItem(value: 'CARRO', label: 'CARRO'),
    PriceType.MOTO: ListItem(value: 'MOTO', label: 'MOTO'),
  };

  static final List<ListItem> PRICE_TYPE_LIST = [
    PRICE_TYPE[PriceType.CARRO]!,
    PRICE_TYPE[PriceType.MOTO]!,
  ];

  static final Map<PriceStatusType, ListItem> PRICE_STATUS = {
    PriceStatusType.ACTIVE: ListItem(value: 'ACTIVE', label: 'Activo'),
    PriceStatusType.INACTIVE: ListItem(value: 'INACTIVE', label: 'Inactivo'),
  };

  static final List<ListItem> PRICE_STATUS_LIST = [
    PriceConsts.PRICE_STATUS[PriceStatusType.ACTIVE]!,
    PriceConsts.PRICE_STATUS[PriceStatusType.INACTIVE]!
  ];
}
