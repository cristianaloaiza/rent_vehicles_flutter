import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rent_vehicles_flutter/bloc/price_bloc.dart';
import 'package:rent_vehicles_flutter/model/price.dart';
import 'package:rent_vehicles_flutter/screen/price/price_const.dart';
import 'package:rent_vehicles_flutter/screen/price/price_form_validator.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';

class PriceCreateScreen extends StatefulWidget {
  final PriceBloc priceBloc;

  PriceCreateScreen({Key? key, required this.priceBloc}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PriceScreen(priceBloc: priceBloc);
}

class _PriceScreen extends State<PriceCreateScreen> {
  final _formKey = GlobalKey<FormState>();
  final _price =
      Price(status: PriceConsts.PRICE_STATUS[PriceStatusType.ACTIVE]?.value);
  final _priceFormValidator = PriceFormValidator();
  final _utils = Utils();
  final PriceBloc priceBloc;
  var _loading = false;

  _PriceScreen({required this.priceBloc});

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void _onCreatePrice() {
      if (_formKey.currentState?.validate() == true) {
        setState(() {
          _loading = true;
        });

        priceBloc.storePrice(_price).then((response) {
          if (response.status == true) {
            _utils.showToastSuccess(response.message);
            _utils.goBack(context);
          } else {
            _utils.showToastError(response.message);
          }

          setState(() {
            _loading = false;
          });
        });
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Crear precio'),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Descripcion'),
                    onChanged: (value) => _price.description = value,
                    validator: _utils.validateString,
                    autofocus: true,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(labelText: 'Valor por dia'),
                    onChanged: (value) =>
                        _price.day_value = double.parse(value),
                    validator: _utils.validateString,
                    inputFormatters: [
                      _priceFormValidator.formatNumber()
                    ],
                  ),
                  SizedBox(height: 10),
                  DropdownButtonFormField<String>(
                    decoration: InputDecoration(labelText: 'Tipo'),
                    value: _price.vehicle_type,
                    onChanged: (String? value) {
                      setState(() {
                        _price.vehicle_type = value!;
                      });
                    },
                    validator: _utils.validateString,
                    items: PriceConsts.PRICE_TYPE_LIST
                        .map<DropdownMenuItem<String>>(
                      (typ) {
                        return DropdownMenuItem<String>(
                          value: typ.value,
                          child: Text(typ.label),
                        );
                      },
                    ).toList(),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _loading ? null : _onCreatePrice,
        tooltip: 'Crear precio',
        backgroundColor: Colors.green,
        child: _loading
            ? CircularProgressIndicator(
                color: Colors.white,
              )
            : Icon(Icons.save_outlined),
      ),
    );
  }
}
