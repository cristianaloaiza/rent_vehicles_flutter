import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/bloc/price_bloc.dart';
import 'package:rent_vehicles_flutter/model/price.dart';
import 'package:rent_vehicles_flutter/screen/price/price_const.dart';
import 'package:rent_vehicles_flutter/screen/price/price_form_validator.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';

class PriceEditScreen extends StatefulWidget {
  final PriceBloc priceBloc;
  final int priceId;

  PriceEditScreen({Key? key, required this.priceBloc, required this.priceId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _PriceScreen(priceBloc: priceBloc, priceId: priceId);
}

class _PriceScreen extends State<PriceEditScreen> {
  final _formKey = GlobalKey<FormState>();
  final _priceFormValidator = PriceFormValidator();
  final _utils = Utils();
  final int priceId;
  final PriceBloc priceBloc;
  late Price _price;
  var _loading = false;
  var _skeleton = true;

  _PriceScreen({required this.priceBloc, required this.priceId}) {
    priceBloc.getPriceById(priceId).then((response) {
      if (response.status == true) {
        _price = response.data;

        setState(() {
          _skeleton = false;
        });
      } else {
        _utils.showToastError(response.message);
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void _onDeletePrice() {
      setState(() {
        _loading = true;
      });

      priceBloc.deletePrice(priceId).then((response) {
        if (response.status == true) {
          _utils.showToastSuccess(response.message);
          _utils.goBack(context);
        } else {
          _utils.showToastError(response.message);
        }

        setState(() {
          _loading = false;
        });
      });
    }

    void _onUpdatePrice() {
      if (_formKey.currentState?.validate() == true) {
        setState(() {
          _loading = true;
        });

        priceBloc.updatePrice(_price).then((response) {
          if (response.status == true) {
            _utils.showToastSuccess(response.message);
          } else {
            _utils.showToastError(response.message);
          }

          setState(() {
            _loading = false;
          });
        });
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: const Text('Editar precio'),
        ),
        body: _skeleton == true
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          SizedBox(height: 10),
                          TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Descripcion'),
                            initialValue: _price.description,
                            onChanged: (value) => _price.description = value,
                            validator: _utils.validateString,
                            autofocus: true,
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(labelText: 'Valor dia'),
                            initialValue: _price.day_value.toString(),
                            onChanged: (value) =>
                                _price.day_value = double.parse(value),
                            validator: _utils.validateString,
                            inputFormatters: [
                              _priceFormValidator.formatNumber()
                            ],
                          ),
                          SizedBox(height: 10),
                          DropdownButtonFormField<String>(
                            decoration: InputDecoration(labelText: 'Tipo'),
                            value: _price.vehicle_type,
                            onChanged: (String? value) {
                              setState(() {
                                _price.vehicle_type = value!;
                              });
                            },
                            validator: _utils.validateString,
                            items: PriceConsts.PRICE_TYPE_LIST
                                .map<DropdownMenuItem<String>>(
                              (typ) {
                                return DropdownMenuItem<String>(
                                  value: typ.value,
                                  child: Text(typ.label),
                                );
                              },
                            ).toList(),
                          ),
                          SizedBox(height: 10),
                          DropdownButtonFormField<String>(
                            decoration: InputDecoration(labelText: 'Estado'),
                            value: _price.status,
                            onChanged: (String? value) {
                              setState(() {
                                _price.status = value!;
                              });
                            },
                            validator: _utils.validateString,
                            items: PriceConsts.PRICE_STATUS_LIST
                                .map<DropdownMenuItem<String>>(
                              (status) {
                                return DropdownMenuItem<String>(
                                  value: status.value,
                                  child: Text(status.label),
                                );
                              },
                            ).toList(),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
        floatingActionButton: _skeleton == true
            ? null
            : Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: FloatingActionButton(
                      onPressed: _loading
                          ? null
                          : () => showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  title: const Text('Eliminar precio'),
                                  content: const Text(
                                      '¿Confirma eliminar el precio?'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(),
                                      child: const Text('Cancelar'),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                        _onDeletePrice();
                                      },
                                      child: const Text('Aceptar'),
                                    ),
                                  ],
                                ),
                              ),
                      tooltip: 'Eliminar precio',
                      backgroundColor: Colors.red,
                      child: _loading
                          ? CircularProgressIndicator(
                              color: Colors.white,
                            )
                          : Icon(Icons.delete),
                    ),
                  ),
                  FloatingActionButton(
                    onPressed: _loading ? null : _onUpdatePrice,
                    tooltip: 'Actualizar precio',
                    backgroundColor: Colors.blue,
                    child: _loading
                        ? CircularProgressIndicator(
                            color: Colors.white,
                          )
                        : Icon(Icons.sync),
                  ),
                ],
              ));
  }
}
