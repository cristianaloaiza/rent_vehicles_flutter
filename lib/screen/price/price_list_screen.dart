import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/bloc/price_bloc.dart';
import 'package:rent_vehicles_flutter/model/price.dart';
import 'package:rent_vehicles_flutter/screen/price/price_const.dart';
import 'package:rent_vehicles_flutter/screen/price/price_create_screen.dart';
import 'package:rent_vehicles_flutter/screen/price/price_edit_screen.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';
import 'package:rent_vehicles_flutter/widget/empty_list_text.dart';

class PriceListScreen extends StatefulWidget {
  const PriceListScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PriceScreen();
}

class _PriceScreen extends State<PriceListScreen> {
  final _priceBloc = PriceBloc();
  final _utils = Utils();
  var _skeleton = true;

  @override
  void initState() {
    super.initState();

    _priceBloc.getAllPrices().then((response) {
      if (response.status == true) {
        setState(() {
          _skeleton = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    PriceType _getRole(str) =>
        PriceType.values.firstWhere((e) => e.toString() == 'PriceType.' + str);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Listado de precios'),
      ),
      body: _skeleton == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : StreamBuilder(
              stream: _priceBloc.priceListStream,
              builder: (
                BuildContext context,
                AsyncSnapshot<List<Price>> snapshot,
              ) {
                final _priceList = snapshot.data ?? [];

                if (_priceList.isEmpty) {
                  return EmptyListText(text: 'Listado de precios vacío');
                }

                return ListView.builder(
                    itemCount: _priceList.length,
                    itemBuilder: (BuildContext context, int index) {
                      final price = _priceList[index];
                      final _isActive = ('PriceStatusType.' + price.status!) ==
                          PriceStatusType.ACTIVE.toString();

                      return ListTile(
                        leading: Icon(
                          Icons.price_change,
                          color: _isActive == true ? Colors.green : Colors.red,
                        ),
                        title: Text(price.description!),
                        subtitle: Text(price.day_value!.toString()),
                        trailing: Chip(
                          label: Text(PriceConsts
                                  .PRICE_TYPE[_getRole(price.vehicle_type)]
                                  ?.label ??
                              ''),
                          backgroundColor: Colors.blue[800],
                          labelStyle: const TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onTap: () => _utils.pushScreen(
                            context,
                            PriceEditScreen(
                              priceBloc: _priceBloc,
                              priceId: price.id!,
                            )),
                      );
                    });
              }),
      floatingActionButton: _skeleton
          ? null
          : FloatingActionButton(
              onPressed: () => _utils.pushScreen(
                  context,
                  PriceCreateScreen(
                    priceBloc: _priceBloc,
                  )),
              tooltip: 'Crear precio',
              backgroundColor: Colors.green,
              child: const Icon(Icons.add),
            ),
    );
  }
}
