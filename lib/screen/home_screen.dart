import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/screen/client/client_list_screen.dart';
import 'package:rent_vehicles_flutter/screen/price/price_list_screen.dart';
import 'package:rent_vehicles_flutter/screen/rentals/rentals_list_screen.dart';
import 'package:rent_vehicles_flutter/screen/vehicle/vehicle_list_screen.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';
import 'package:rent_vehicles_flutter/widget/button.dart';

class HomeScreen extends StatelessWidget {
  final _utils = Utils();

  HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _navigate(Widget screen) => _utils.pushScreen(context, screen);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Sistema de Gestión de Proyectos'),
      ),
      body: Container(
        width: double.infinity,
        child: Center(
          child: Padding(
            padding: EdgeInsets.all(40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/images/logo_app.png',
                  width: 125,
                  height: 125,
                ),
                SizedBox(height: 20),
                Button(
                    text: 'Gestión de Clientes',
                    onPressed: () => _navigate(const ClientListScreen()),
                    icon: Icons.people),
                SizedBox(height: 10),
                Button(
                    text: 'Gestión de Vehículos',
                    onPressed: () => _navigate(const VehicleListScreen()),
                    icon: Icons.car_repair),
                SizedBox(height: 10),
                Button(
                    text: 'Gestión de Precios',
                    onPressed: () => _navigate(const PriceListScreen()),
                    icon: Icons.price_change),
                SizedBox(height: 10),
                Button(
                    text: 'Gestión de Alquileres',
                    onPressed: () => _navigate(const RentalsListScreen()),
                    icon: Icons.calendar_today),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
