import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/bloc/rentals_bloc.dart';
import 'package:rent_vehicles_flutter/model/rentals.dart';
import 'package:rent_vehicles_flutter/screen/rentals/rentals_const.dart';
import 'package:rent_vehicles_flutter/screen/rentals/rentals_create_screen.dart';
import 'package:rent_vehicles_flutter/screen/rentals/rentals_edit_screen.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';
import 'package:rent_vehicles_flutter/widget/empty_list_text.dart';

class RentalsListScreen extends StatefulWidget {
  const RentalsListScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _RentalsScreen();
}

class _RentalsScreen extends State<RentalsListScreen> {
  final _rentalsBloc = RentalsBloc();
  final _utils = Utils();
  var _skeleton = true;

  @override
  void initState() {
    super.initState();

    _rentalsBloc.getAllRentals().then((response) {
      if (response.status == true) {
        setState(() {
          _skeleton = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Listado de alquileres'),
      ),
      body: _skeleton == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : StreamBuilder(
              stream: _rentalsBloc.rentalsListStream,
              builder: (
                BuildContext context,
                AsyncSnapshot<List<Rentals>> snapshot,
              ) {
                final _rentalsList = snapshot.data ?? [];

                if (_rentalsList.isEmpty) {
                  return EmptyListText(text: 'Listado de alquileres vacío');
                }

                return ListView.builder(
                    itemCount: _rentalsList.length,
                    itemBuilder: (BuildContext context, int index) {
                      final rentals = _rentalsList[index];
                      final _isActive =
                          ('RentalsStatusType.' + rentals.status!) ==
                              RentalsStatusType.ACTIVE.toString();

                      return ListTile(
                        leading: Icon(
                          Icons.person,
                          color: _isActive == true ? Colors.green : Colors.red,
                        ),
                        title: Text(rentals.start_date!),
                        subtitle: Text(rentals.final_date!),
                        trailing: Chip(
                          label: Text(rentals.total.toString()),
                          backgroundColor: Colors.blue[800],
                          labelStyle: const TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onTap: () => _utils.pushScreen(
                            context,
                            RentalsEditScreen(
                              rentalsBloc: _rentalsBloc,
                              rentalsId: rentals.id!,
                            )),
                      );
                    });
              }),
      floatingActionButton: _skeleton
          ? null
          : FloatingActionButton(
              onPressed: () => _utils.pushScreen(
                  context,
                  RentalsCreateScreen(
                    rentalsBloc: _rentalsBloc,
                  )),
              tooltip: 'Crear alquiler',
              backgroundColor: Colors.green,
              child: const Icon(Icons.add),
            ),
    );
  }
}
