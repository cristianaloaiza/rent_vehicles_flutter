import 'package:date_field/date_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent_vehicles_flutter/bloc/client_bloc.dart';
import 'package:rent_vehicles_flutter/bloc/price_bloc.dart';
import 'package:rent_vehicles_flutter/bloc/rentals_bloc.dart';
import 'package:rent_vehicles_flutter/bloc/vehicle_bloc.dart';
import 'package:rent_vehicles_flutter/model/client.dart';
import 'package:rent_vehicles_flutter/model/price.dart';
import 'package:rent_vehicles_flutter/model/rentals.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart';
import 'package:rent_vehicles_flutter/screen/client/client_form_validator.dart';
import 'package:rent_vehicles_flutter/utils/utils.dart';

class RentalsEditScreen extends StatefulWidget {
  final RentalsBloc rentalsBloc;
  final int rentalsId;

  RentalsEditScreen(
      {Key? key, required this.rentalsBloc, required this.rentalsId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _RentalsScreen(rentalsBloc: rentalsBloc, rentalsId: rentalsId);
}

class _RentalsScreen extends State<RentalsEditScreen> {
  final _formKey = GlobalKey<FormState>();
  final _clientFormValidator = ClientFormValidator();
  final _utils = Utils();
  final RentalsBloc rentalsBloc;
  late Rentals _rentals;
  final int rentalsId;
  final _vehicleBloc = VehicleBloc();
  final _clientBloc = ClientBloc();
  final _priceBloc = PriceBloc();
  var _skeleton = true;

  var _loading = false;

  _RentalsScreen({required this.rentalsBloc, required this.rentalsId}) {
    rentalsBloc.getRentalsById(rentalsId).then((response) {
      if (response.status == true) {
        _rentals = response.data;

        setState(() {
          _skeleton = false;
        });
      } else {
        _utils.showToastError(response.message);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _vehicleBloc.getAllVehicles().then((response) {
      if (response.status == true) {
        setState(() {
          _skeleton = false;
        });
      }
    });
    _clientBloc.getAllClients().then((response) {
      if (response.status == true) {
        setState(() {
          _skeleton = false;
        });
      }
    });
    _priceBloc.getAllPrices().then((response) {
      if (response.status == true) {
        setState(() {
          _skeleton = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    void _onDeleteRentals() {
      setState(() {
        _loading = true;
      });

      rentalsBloc.deleteRentals(_rentals.id!).then((response) {
        if (response.status == true) {
          _utils.showToastSuccess(response.message);
          _utils.goBack(context);
        } else {
          _utils.showToastError(response.message);
        }

        setState(() {
          _loading = false;
        });
      });
    }

    void _onUpdateRentals() {
      if (_formKey.currentState?.validate() == true) {
        setState(() {
          _loading = true;
        });

        rentalsBloc.updateRentals(_rentals).then((response) {
          if (response.status == true) {
            _utils.showToastSuccess(response.message);
          } else {
            _utils.showToastError(response.message);
          }

          setState(() {
            _loading = false;
          });
        });
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: const Text('Editar alquiler'),
        ),
        body: _skeleton == true
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          StreamBuilder(
                              stream: _clientBloc.clientListStream,
                              builder: (
                                BuildContext context,
                                AsyncSnapshot<List<Client>> snapshot,
                              ) {
                                final clientList = snapshot.data ?? [];

                                return DropdownButtonFormField<int>(
                                  autofocus: true,
                                  decoration:
                                      InputDecoration(labelText: 'Cliente'),
                                  value: _rentals.id_client,
                                  validator: (value) => _utils.validateString(
                                      value == null ? '' : value.toString()),
                                  onChanged: (int? value) {
                                    setState(() {
                                      _rentals.id_client = value!;
                                    });
                                  },
                                  items: clientList.map<DropdownMenuItem<int>>(
                                    (client) {
                                      return DropdownMenuItem<int>(
                                        value: client.id,
                                        child: Text(client.name.toString()),
                                      );
                                    },
                                  ).toList(),
                                );
                              }),
                          SizedBox(height: 10),
                          StreamBuilder(
                              stream: _vehicleBloc.vehicleListStream,
                              builder: (
                                BuildContext context,
                                AsyncSnapshot<List<Vehicle>> snapshot,
                              ) {
                                final vehicleList = snapshot.data ?? [];

                                return DropdownButtonFormField<int>(
                                  decoration:
                                      InputDecoration(labelText: 'Vehículo'),
                                  value: _rentals.id_vehicle,
                                  validator: (value) => _utils.validateString(
                                      value == null ? '' : value.toString()),
                                  onChanged: (int? value) {
                                    setState(() {
                                      _rentals.id_vehicle = value!;
                                    });
                                  },
                                  items: vehicleList.map<DropdownMenuItem<int>>(
                                    (vehicle) {
                                      return DropdownMenuItem<int>(
                                        value: vehicle.id,
                                        child: Text(vehicle.type.toString() +
                                            ' ' +
                                            vehicle.model.toString()),
                                      );
                                    },
                                  ).toList(),
                                );
                              }),
                          SizedBox(height: 10),
                          StreamBuilder(
                              stream: _priceBloc.priceListStream,
                              builder: (
                                BuildContext context,
                                AsyncSnapshot<List<Price>> snapshot,
                              ) {
                                final priceList = snapshot.data ?? [];

                                return DropdownButtonFormField<int>(
                                  decoration:
                                      InputDecoration(labelText: 'Precio'),
                                  value: _rentals.id_price,
                                  validator: (value) => _utils.validateString(
                                      value == null ? '' : value.toString()),
                                  onChanged: (int? value) {
                                    setState(() {
                                      _rentals.id_price = value!;
                                    });
                                  },
                                  items: priceList.map<DropdownMenuItem<int>>(
                                    (price) {
                                      return DropdownMenuItem<int>(
                                        value: price.id,
                                        child: Text(price.day_value.toString()),
                                      );
                                    },
                                  ).toList(),
                                );
                              }),
                          SizedBox(height: 10),
                          DateTimeFormField(
                            decoration: const InputDecoration(
                              hintStyle: TextStyle(color: Colors.black45),
                              errorStyle: TextStyle(color: Colors.redAccent),
                              suffixIcon: Icon(Icons.event_note),
                              labelText: 'Fecha de inicio',
                            ),
                            initialValue: DateTime.parse(_rentals.start_date!),
                            validator: _utils.validateDate,
                            mode: DateTimeFieldPickerMode.date,
                            onDateSelected: (value) => _rentals.start_date =
                                value.toIso8601String().toString(),
                          ),
                          SizedBox(height: 10),
                          DateTimeFormField(
                            decoration: const InputDecoration(
                              hintStyle: TextStyle(color: Colors.black45),
                              errorStyle: TextStyle(color: Colors.redAccent),
                              suffixIcon: Icon(Icons.event_note),
                              labelText: 'Fecha de finalización',
                            ),
                            initialValue: DateTime.parse(_rentals.final_date!),
                            validator: _utils.validateDate,
                            mode: DateTimeFieldPickerMode.date,
                            onDateSelected: (value) => _rentals.final_date =
                                value.toIso8601String().toString(),
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(labelText: 'Total'),
                            onChanged: (value) =>
                                _rentals.total = double.parse(value),
                            validator: _utils.validateString,
                            initialValue: _rentals.total.toString(),
                            inputFormatters: [
                              _clientFormValidator.formatDocumentNumber()
                            ],
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            initialValue: _rentals.observations,
                            decoration:
                                InputDecoration(labelText: 'Observaciones'),
                            onChanged: (value) => _rentals.observations = value,
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
        floatingActionButton: _skeleton == true
            ? null
            : Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: FloatingActionButton(
                      onPressed: _loading
                          ? null
                          : () => showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  title: const Text('Eliminar alquiler'),
                                  content: const Text(
                                      '¿Confirma eliminar el alquiler?'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(),
                                      child: const Text('Cancelar'),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                        _onDeleteRentals();
                                      },
                                      child: const Text('Aceptar'),
                                    ),
                                  ],
                                ),
                              ),
                      tooltip: 'Eliminar alquiler',
                      backgroundColor: Colors.red,
                      child: _loading
                          ? CircularProgressIndicator(
                              color: Colors.white,
                            )
                          : Icon(Icons.delete),
                    ),
                  ),
                  FloatingActionButton(
                    onPressed: _loading ? null : _onUpdateRentals,
                    tooltip: 'Actualizar alquiler',
                    backgroundColor: Colors.blue,
                    child: _loading
                        ? CircularProgressIndicator(
                            color: Colors.white,
                          )
                        : Icon(Icons.sync),
                  ),
                ],
              ));
  }
}
