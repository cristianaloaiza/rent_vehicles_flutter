// coverage:ignore-file
import 'package:rent_vehicles_flutter/utils/utils.dart';

enum RentalsStatusType { ACTIVE, INACTIVE }

class RentalsConsts {

  static final Map<RentalsStatusType, ListItem> RENTALS_STATUS = {
    RentalsStatusType.ACTIVE: ListItem(value: 'ACTIVE', label: 'Activo'),
    RentalsStatusType.INACTIVE: ListItem(value: 'INACTIVE', label: 'Inactivo'),
  };

  static final List<ListItem> CLIENT_STATUS_LIST = [
    RentalsConsts.RENTALS_STATUS[RentalsStatusType.ACTIVE]!,
    RentalsConsts.RENTALS_STATUS[RentalsStatusType.INACTIVE]!
  ];
}