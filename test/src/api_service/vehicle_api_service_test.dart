import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:rent_vehicles_flutter/api_service/vehicle_api_service.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart' as vehicle_model;

void main() {
  final vehicleToStore = vehicle_model.Vehicle(
    id: 1,
    type: 'MOTO',
    model: '2000',
    cylinder_capacity: '125cc',
    colour: 'Rojo',
    plate: 'ALF56Y',
    mileage: '2500',
    status: 'ACTIVE',
    observations: 'Registro por primera vez',
  );

  final vehicle = vehicle_model.Vehicle(
    id: 1,
    type: 'MOTO',
    model: '2000',
    cylinder_capacity: '125cc',
    colour: 'Rojo',
    plate: 'ALF56Y',
    mileage: '2500',
    status: 'ACTIVE',
    observations: 'Registro por primera vez',
  );

  group('VEHICLE API SERVICE TEST', () {
    group('GET ALL VEHICLES', () {
      test('OK', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          final vehicleList = [vehicle, vehicle];

          return Response(
              jsonEncode({
                'status': true,
                'data': {'vehicles': vehicleList}
              }),
              200);
        });

        final apiResponse = await vehicleApiService.getAllVehicles();

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await vehicleApiService.getAllVehicles();

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('GET VEHICLE BY ID', () {
      test('OK', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'vehicle': vehicle}
              }),
              200);
        });

        final apiResponse = await vehicleApiService.getVehicleById(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT FOUND', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await vehicleApiService.getVehicleById(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await vehicleApiService.getVehicleById(1);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('STORE VEHICLE', () {
      test('OK', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'vehicle': vehicle}
              }),
              200);
        });

        final apiResponse =
            await vehicleApiService.storeVehicle(vehicleToStore);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT CREATED', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse =
            await vehicleApiService.storeVehicle(vehicleToStore);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse =
            await vehicleApiService.storeVehicle(vehicleToStore);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('UPDATE VEHICLE', () {
      test('OK', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'vehicle': vehicle}
              }),
              200);
        });

        final apiResponse = await vehicleApiService.updateVehicle(vehicle);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT UPDATED', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await vehicleApiService.updateVehicle(vehicle);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await vehicleApiService.updateVehicle(vehicle);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('DELETE VEHICLE', () {
      test('OK', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': true, 'message': 'Deleted'}), 200);
        });

        final apiResponse = await vehicleApiService.deleteVehicle(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT DELETED', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await vehicleApiService.deleteVehicle(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final vehicleApiService = VehicleApiService();

        vehicleApiService.vehicleHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await vehicleApiService.deleteVehicle(1);

        expect(apiResponse, isA<ApiResponse>());
      });
    });
  });
}
