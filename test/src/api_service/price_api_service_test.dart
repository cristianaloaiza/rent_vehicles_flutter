import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:rent_vehicles_flutter/api_service/price_api_service.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/price.dart' as price_model;

void main() {
  final priceToStore = price_model.Price(
    id: 1,
    description: 'Valor por 1 dia',
    day_value: 2500,
    vehicle_type: 'MOTO',
    status: 'ACTIVE',
  );

  final price = price_model.Price(
    id: 1,
    description: 'Valor por 1 dia',
    day_value: 2500,
    vehicle_type: 'MOTO',
    status: 'ACTIVE',
  );

  group('PRICE API SERVICE TEST', () {
    group('GET ALL PRICES', () {
      test('OK', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          final priceList = [price, price];

          return Response(
              jsonEncode({
                'status': true,
                'data': {'prices': priceList}
              }),
              200);
        });

        final apiResponse = await priceApiService.getAllPrices();

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await priceApiService.getAllPrices();

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('GET PRICE BY ID', () {
      test('OK', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'price': price}
              }),
              200);
        });

        final apiResponse = await priceApiService.getPriceById(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT FOUND', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await priceApiService.getPriceById(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await priceApiService.getPriceById(1);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('STORE PRICE', () {
      test('OK', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'price': price}
              }),
              200);
        });

        final apiResponse = await priceApiService.storePrice(priceToStore);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT CREATED', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await priceApiService.storePrice(priceToStore);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await priceApiService.storePrice(priceToStore);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('UPDATE PRICE', () {
      test('OK', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'price': price}
              }),
              200);
        });

        final apiResponse = await priceApiService.updatePrice(price);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT UPDATED', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await priceApiService.updatePrice(price);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await priceApiService.updatePrice(price);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('DELETE PRICE', () {
      test('OK', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': true, 'message': 'Deleted'}), 200);
        });

        final apiResponse = await priceApiService.deletePrice(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT DELETED', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await priceApiService.deletePrice(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final priceApiService = PriceApiService();

        priceApiService.priceHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await priceApiService.deletePrice(1);

        expect(apiResponse, isA<ApiResponse>());
      });
    });
  });
}
