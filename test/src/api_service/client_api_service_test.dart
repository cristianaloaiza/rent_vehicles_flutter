import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:rent_vehicles_flutter/api_service/client_api_service.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/client.dart' as client_model;

void main() {
  final clientToStore = client_model.Client(
    id: 1,
    document_number: '123',
    name: 'Cristian Loaiza',
    email: 'cl@cl.cl',
    birthdate: '2021-01-01',
    license: 'A1',
    status: 'ACTIVE',
  );

  final client = client_model.Client(
    id: 1,
    document_number: '123',
    name: 'Cristian Loaiza',
    email: 'cl@cl.cl',
    birthdate: '2021-01-01',
    license: 'A1',
    status: 'ACTIVE',
  );

  group('CLIENT API SERVICE TEST', () {
    group('GET ALL CLIENTS', () {
      test('OK', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          final clientList = [client, client];

          return Response(
              jsonEncode({
                'status': true,
                'data': {'clients': clientList}
              }),
              200);
        });

        final apiResponse = await clientApiService.getAllClients();

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await clientApiService.getAllClients();

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('GET CLIENT BY ID', () {
      test('OK', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'client': client}
              }),
              200);
        });

        final apiResponse = await clientApiService.getClientById(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT FOUND', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await clientApiService.getClientById(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await clientApiService.getClientById(1);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('STORE CLIENT', () {
      test('OK', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'client': client}
              }),
              200);
        });

        final apiResponse = await clientApiService.storeClient(clientToStore);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT CREATED', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await clientApiService.storeClient(clientToStore);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await clientApiService.storeClient(clientToStore);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('UPDATE CLIENT', () {
      test('OK', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'client': client}
              }),
              200);
        });

        final apiResponse = await clientApiService.updateClient(client);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT UPDATED', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await clientApiService.updateClient(client);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await clientApiService.updateClient(client);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('DELETE CLIENT', () {
      test('OK', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': true, 'message': 'Deleted'}), 200);
        });

        final apiResponse = await clientApiService.deleteClient(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT DELETED', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await clientApiService.deleteClient(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final clientApiService = ClientApiService();

        clientApiService.clientHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await clientApiService.deleteClient(1);

        expect(apiResponse, isA<ApiResponse>());
      });
    });
  });
}
