import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:rent_vehicles_flutter/api_service/rentals_api_service.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/rentals.dart' as rentals_model;

void main() {
  final rentalsToStore = rentals_model.Rentals(
    id: 1,
    id_vehicle: 1,
    id_client: 1,
    id_price: 1,
    start_date: '2021-01-01T05:00:00.000Z',
    final_date: '2021-01-01T05:00:00.000Z',
    total: 20000,
    status: 'ACTIVE',
    observations: 'NIGUNA',
  );

  final rentals = rentals_model.Rentals(
    id: 1,
    id_vehicle: 1,
    id_client: 1,
    id_price: 1,
    start_date: '2021-01-01T05:00:00.000Z',
    final_date: '2021-01-01T05:00:00.000Z',
    total: 20000,
    status: 'ACTIVE',
    observations: 'NIGUNA',
  );

  group('RENTALS API SERVICE TEST', () {
    group('GET ALL RENTALS', () {
      test('OK', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          final rentalsList = [rentals, rentals];

          return Response(
              jsonEncode({
                'status': true,
                'data': {'rentals': rentalsList}
              }),
              200);
        });

        final apiResponse = await rentalsApiService.getAllRentals();

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await rentalsApiService.getAllRentals();

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('GET RENTALS BY ID', () {
      test('OK', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'rentals': rentals}
              }),
              200);
        });

        final apiResponse = await rentalsApiService.getRentalsById(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT FOUND', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await rentalsApiService.getRentalsById(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await rentalsApiService.getRentalsById(1);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('STORE RENTALS', () {
      test('OK', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'rentals': rentals}
              }),
              200);
        });

        final apiResponse = await rentalsApiService.storeRentals(rentalsToStore);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT CREATED', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await rentalsApiService.storeRentals(rentalsToStore);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await rentalsApiService.storeRentals(rentalsToStore);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('UPDATE RENTALS', () {
      test('OK', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({
                'status': true,
                'data': {'rentals': rentals}
              }),
              200);
        });

        final apiResponse = await rentalsApiService.updateRentals(rentals);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT UPDATED', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await rentalsApiService.updateRentals(rentals);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await rentalsApiService.updateRentals(rentals);

        expect(apiResponse, isA<ApiResponse>());
      });
    });

    group('DELETE RENTALS', () {
      test('OK', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client  = MockClient((request) async {
          return Response(
              jsonEncode({'status': true, 'message': 'Deleted'}), 200);
        });

        final apiResponse = await rentalsApiService.deleteRentals(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('NOT DELETED', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client  = MockClient((request) async {
          return Response(
              jsonEncode({'status': false, 'message': 'Error'}), 400);
        });

        final apiResponse = await rentalsApiService.deleteRentals(1);

        expect(apiResponse, isA<ApiResponse>());
      });

      test('ERROR', () async {
        final rentalsApiService = RentalsApiService();

        rentalsApiService.rentalsHelper.client = MockClient((request) async {
          return Response(jsonEncode(null), 400);
        });

        final apiResponse = await rentalsApiService.deleteRentals(1);

        expect(apiResponse, isA<ApiResponse>());
      });
    });
  });
}
