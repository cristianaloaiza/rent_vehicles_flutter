import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:rent_vehicles_flutter/api_service/client_api_service.dart';
import 'package:rent_vehicles_flutter/bloc/client_bloc.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/client.dart';

import 'client_bloc_test.mocks.dart';

// flutter pub run build_runner build
@GenerateMocks([ClientApiService])
void main() {
  group('CLIENT BLOC TEST', () {
    final clientBloc = ClientBloc();

    clientBloc.clientRepository.clientApiService = MockClientApiService();

    final clientToCreate = Client(
        document_number: '123',
        name: 'Cristian',
        email: 'cl@cl.cl',
        birthdate: '2021-01-01',
        license: 'A1');

    final client = Client(
        id: 1,
        document_number: '123',
        name: 'Cristian',
        email: 'cl@cl.cl',
        birthdate: '2021-01-01',
        license: 'A1',
        status: 'ACTIVE');

    group('GET ALL CLIENTS', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: <Client>[client, client]),
        );

        when(clientBloc.clientRepository.getAllClients())
            .thenAnswer((_) => response);

        expect(clientBloc.getAllClients(), isA<Future<ApiResponse>>());
      });

      test('EMPTY LIST', () async {
        final response = Future.value(
          ApiResponse(status: true, data: <Client>[]),
        );

        when(clientBloc.clientRepository.getAllClients())
            .thenAnswer((_) => response);

        expect(clientBloc.getAllClients(), isA<Future<ApiResponse>>());
      });

      test('ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(clientBloc.clientRepository.getAllClients())
            .thenAnswer((_) => response);

        expect(clientBloc.getAllClients(), isA<Future<ApiResponse>>());
      });
    });

    group('GET CLIENT BY ID', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: client),
        );

        when(clientBloc.clientRepository.getClientById(1))
            .thenAnswer((_) => response);

        expect(clientBloc.getClientById(1), isA<Future<ApiResponse>>());
      });

      test('NOT FOUND/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(clientBloc.clientRepository.getClientById(1))
            .thenAnswer((_) => response);

        expect(clientBloc.getClientById(1), isA<Future<ApiResponse>>());
      });
    });

    group('STORE CLIENT', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: client),
        );

        when(clientBloc.clientRepository.storeClient(clientToCreate))
            .thenAnswer((_) => response);

        expect(
            clientBloc.storeClient(clientToCreate), isA<Future<ApiResponse>>());
      });

      test('NOT CREATED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(clientBloc.clientRepository.storeClient(clientToCreate))
            .thenAnswer((_) => response);

        expect(
            clientBloc.storeClient(clientToCreate), isA<Future<ApiResponse>>());
      });
    });

    group('UPDATE CLIENT', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Updated'),
        );

        when(clientBloc.clientRepository.updateClient(client))
            .thenAnswer((_) => response);

        expect(clientBloc.updateClient(client), isA<Future<ApiResponse>>());
      });

      test('NOT FOUND', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Updated'),
        );

        when(clientBloc.clientRepository.updateClient(clientToCreate))
            .thenAnswer((_) => response);

        expect(clientBloc.updateClient(clientToCreate),
            isA<Future<ApiResponse>>());
      });

      test('NOT UPDATED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(clientBloc.clientRepository.updateClient(client))
            .thenAnswer((_) => response);

        expect(clientBloc.updateClient(client), isA<Future<ApiResponse>>());
      });
    });

    group('DELETE CLIENT', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Deleted'),
        );

        when(clientBloc.clientRepository.deleteClient(1))
            .thenAnswer((_) => response);

        expect(clientBloc.deleteClient(1), isA<Future<ApiResponse>>());
      });

      test('NOT DELETED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(clientBloc.clientRepository.deleteClient(1))
            .thenAnswer((_) => response);

        expect(clientBloc.deleteClient(1), isA<Future<ApiResponse>>());
      });
    });

    test('SINGLE METHODS', () {
      clientBloc.setClient(client);
      clientBloc.setClientList([client, client]);

      expect(clientBloc.getClient(), isA<Client>());
      expect(clientBloc.getClientList(), isA<List<Client>>());

      expect(clientBloc.clientListStream, isA<Stream<List<Client>>>());
      expect(clientBloc.clientStream, isA<Stream<Client>>());

      clientBloc.dispose();
    });
  });
}
