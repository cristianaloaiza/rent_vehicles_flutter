// flutter pub run build_runner build
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:rent_vehicles_flutter/api_service/rentals_api_service.dart';
import 'package:rent_vehicles_flutter/bloc/rentals_bloc.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/rentals.dart';

import 'rentals_bloc_test.mocks.dart';
@GenerateMocks([RentalsApiService])
void main() {
  group('RENTALS BLOC TEST', () {
    final rentalsBloc = RentalsBloc();

    rentalsBloc.rentalsRepository.rentalsApiService = MockRentalsApiService();

  final rentalsToCreate = Rentals(
    id_vehicle: 1,
    id_client: 1,
    id_price: 1,
    start_date: '2021-01-01T05:00:00.000Z',
    final_date: '2021-01-01T05:00:00.000Z',
    total: 20000,
    status: 'ACTIVE',
    observations: 'NIGUNA',
  );

  final rentals = Rentals(
    id: 1,
    id_vehicle: 1,
    id_client: 1,
    id_price: 1,
    start_date: '2021-01-01T05:00:00.000Z',
    final_date: '2021-01-01T05:00:00.000Z',
    total: 20000,
    status: 'ACTIVE',
    observations: 'NIGUNA',
  );

    group('GET ALL RENTALS', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: <Rentals>[rentals, rentals]),
        );

        when(rentalsBloc.rentalsRepository.getAllRentals())
            .thenAnswer((_) => response);

        expect(rentalsBloc.getAllRentals(), isA<Future<ApiResponse>>());
      });

      test('EMPTY LIST', () async {
        final response = Future.value(
          ApiResponse(status: true, data: <Rentals>[]),
        );

        when(rentalsBloc.rentalsRepository.getAllRentals())
            .thenAnswer((_) => response);

        expect(rentalsBloc.getAllRentals(), isA<Future<ApiResponse>>());
      });

      test('ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(rentalsBloc.rentalsRepository.getAllRentals())
            .thenAnswer((_) => response);

        expect(rentalsBloc.getAllRentals(), isA<Future<ApiResponse>>());
      });
    });

    group('GET RENTALS BY ID', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: rentals),
        );

        when(rentalsBloc.rentalsRepository.getRentalsById(1))
            .thenAnswer((_) => response);

        expect(rentalsBloc.getRentalsById(1), isA<Future<ApiResponse>>());
      });

      test('NOT FOUND/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(rentalsBloc.rentalsRepository.getRentalsById(1))
            .thenAnswer((_) => response);

        expect(rentalsBloc.getRentalsById(1), isA<Future<ApiResponse>>());
      });
    });

    group('STORE RENTALS', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: rentals),
        );

        when(rentalsBloc.rentalsRepository.storeRentals(rentalsToCreate))
            .thenAnswer((_) => response);

        expect(
            rentalsBloc.storeRentals(rentalsToCreate), isA<Future<ApiResponse>>());
      });

      test('NOT CREATED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(rentalsBloc.rentalsRepository.storeRentals(rentalsToCreate))
            .thenAnswer((_) => response);

        expect(
            rentalsBloc.storeRentals(rentalsToCreate), isA<Future<ApiResponse>>());
      });
    });

    group('UPDATE RENTALS', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Updated'),
        );

        when(rentalsBloc.rentalsRepository.updateRentals(rentals))
            .thenAnswer((_) => response);

        expect(rentalsBloc.updateRentals(rentals), isA<Future<ApiResponse>>());
      });

      test('NOT FOUND', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Updated'),
        );

        when(rentalsBloc.rentalsRepository.updateRentals(rentalsToCreate))
            .thenAnswer((_) => response);

        expect(rentalsBloc.updateRentals(rentalsToCreate),
            isA<Future<ApiResponse>>());
      });

      test('NOT UPDATED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(rentalsBloc.rentalsRepository.updateRentals(rentals))
            .thenAnswer((_) => response);

        expect(rentalsBloc.updateRentals(rentals), isA<Future<ApiResponse>>());
      });
    });

    group('DELETE RENTALS', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Deleted'),
        );

        when(rentalsBloc.rentalsRepository.deleteRentals(1))
            .thenAnswer((_) => response);

        expect(rentalsBloc.deleteRentals(1), isA<Future<ApiResponse>>());
      });

      test('NOT DELETED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(rentalsBloc.rentalsRepository.deleteRentals(1))
            .thenAnswer((_) => response);

        expect(rentalsBloc.deleteRentals(1), isA<Future<ApiResponse>>());
      });
    });

    test('SINGLE METHODS', () {
      rentalsBloc.setRentals(rentals);
      rentalsBloc.setRentalsList([rentals, rentals]);

      expect(rentalsBloc.getRentals(), isA<Rentals>());
      expect(rentalsBloc.getRentalsList(), isA<List<Rentals>>());

      expect(rentalsBloc.rentalsListStream, isA<Stream<List<Rentals>>>());
      expect(rentalsBloc.rentalsStream, isA<Stream<Rentals>>());

      rentalsBloc.dispose();
    });
  });
}
