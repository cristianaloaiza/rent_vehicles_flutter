import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:rent_vehicles_flutter/api_service/vehicle_api_service.dart';
import 'package:rent_vehicles_flutter/bloc/vehicle_bloc.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/vehicle.dart';

import 'vehicle_bloc_test.mocks.dart';

// flutter pub run build_runner build
@GenerateMocks([VehicleApiService])
void main() {
  group('VEHICLE BLOC TEST', () {
    final vehicleBloc = VehicleBloc();

    vehicleBloc.vehicleRepository.vehicleApiService = MockVehicleApiService();

    final vehicleToCreate = Vehicle(
      type: 'MOTO',
      model: '2000',
      cylinder_capacity: '125cc',
      colour: 'Rojo',
      plate: 'ALF56Y',
      mileage: '2500',
      observations: 'Registro por primera vez',
    );

    final vehicle = Vehicle(
        id: 1,
        type: 'MOTO',
        model: '2000',
        cylinder_capacity: '125cc',
        colour: 'Rojo',
        plate: 'ALF56Y',
        mileage: '2500',
        status: 'ACTIVE',
        observations: 'Registro por primera vez');

    group('GET ALL VEHICLES', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: <Vehicle>[vehicle, vehicle]),
        );

        when(vehicleBloc.vehicleRepository.getAllVehicles())
            .thenAnswer((_) => response);

        expect(vehicleBloc.getAllVehicles(), isA<Future<ApiResponse>>());
      });

      test('EMPTY LIST', () async {
        final response = Future.value(
          ApiResponse(status: true, data: <Vehicle>[]),
        );

        when(vehicleBloc.vehicleRepository.getAllVehicles())
            .thenAnswer((_) => response);

        expect(vehicleBloc.getAllVehicles(), isA<Future<ApiResponse>>());
      });

      test('ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(vehicleBloc.vehicleRepository.getAllVehicles())
            .thenAnswer((_) => response);

        expect(vehicleBloc.getAllVehicles(), isA<Future<ApiResponse>>());
      });
    });

    group('GET VEHICLE BY ID', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: vehicle),
        );

        when(vehicleBloc.vehicleRepository.getVehicleById(1))
            .thenAnswer((_) => response);

        expect(vehicleBloc.getVehicleById(1), isA<Future<ApiResponse>>());
      });

      test('NOT FOUND/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(vehicleBloc.vehicleRepository.getVehicleById(1))
            .thenAnswer((_) => response);

        expect(vehicleBloc.getVehicleById(1), isA<Future<ApiResponse>>());
      });
    });

    group('STORE VEHICLE', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: vehicle),
        );

        when(vehicleBloc.vehicleRepository.storeVehicle(vehicleToCreate))
            .thenAnswer((_) => response);

        expect(vehicleBloc.storeVehicle(vehicleToCreate),
            isA<Future<ApiResponse>>());
      });

      test('NOT CREATED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(vehicleBloc.vehicleRepository.storeVehicle(vehicleToCreate))
            .thenAnswer((_) => response);

        expect(vehicleBloc.storeVehicle(vehicleToCreate),
            isA<Future<ApiResponse>>());
      });
    });

    group('UPDATE VEHICLE', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Updated'),
        );

        when(vehicleBloc.vehicleRepository.updateVehicle(vehicle))
            .thenAnswer((_) => response);

        expect(vehicleBloc.updateVehicle(vehicle), isA<Future<ApiResponse>>());
      });

      test('NOT FOUND', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Updated'),
        );

        when(vehicleBloc.vehicleRepository.updateVehicle(vehicleToCreate))
            .thenAnswer((_) => response);

        expect(vehicleBloc.updateVehicle(vehicleToCreate),
            isA<Future<ApiResponse>>());
      });

      test('NOT UPDATED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(vehicleBloc.vehicleRepository.updateVehicle(vehicle))
            .thenAnswer((_) => response);

        expect(vehicleBloc.updateVehicle(vehicle), isA<Future<ApiResponse>>());
      });
    });

    group('DELETE VEHICLE', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Deleted'),
        );

        when(vehicleBloc.vehicleRepository.deleteVehicle(1))
            .thenAnswer((_) => response);

        expect(vehicleBloc.deleteVehicle(1), isA<Future<ApiResponse>>());
      });

      test('NOT DELETED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(vehicleBloc.vehicleRepository.deleteVehicle(1))
            .thenAnswer((_) => response);

        expect(vehicleBloc.deleteVehicle(1), isA<Future<ApiResponse>>());
      });
    });

    test('SINGLE METHODS', () {
      vehicleBloc.setVehicle(vehicle);
      vehicleBloc.setVehicleList([vehicle, vehicle]);

      expect(vehicleBloc.getVehicle(), isA<Vehicle>());
      expect(vehicleBloc.getVehicleList(), isA<List<Vehicle>>());

      expect(vehicleBloc.vehicleListStream, isA<Stream<List<Vehicle>>>());
      expect(vehicleBloc.vehicleStream, isA<Stream<Vehicle>>());

      vehicleBloc.dispose();
    });
  });
}
