import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:rent_vehicles_flutter/api_service/price_api_service.dart';
import 'package:rent_vehicles_flutter/bloc/price_bloc.dart';
import 'package:rent_vehicles_flutter/model/api_response_model.dart';
import 'package:rent_vehicles_flutter/model/price.dart';

import 'price_bloc_test.mocks.dart';

// flutter pub run build_runner build
@GenerateMocks([PriceApiService])
void main() {
  group('PRICE BLOC TEST', () {
    final priceBloc = PriceBloc();

    priceBloc.priceRepository.priceApiService = MockPriceApiService();

    final priceToCreate = Price(
      description: 'Valor por 1 dia',
      day_value: 2500,
      vehicle_type: 'MOTO',
    );

    final price = Price(
        id: 1,
        description: 'Valor por 1 dia',
        day_value: 2500,
        vehicle_type: 'MOTO',
        status: 'ACTIVE');

    group('GET ALL PRICES', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: <Price>[price, price]),
        );

        when(priceBloc.priceRepository.getAllPrices())
            .thenAnswer((_) => response);

        expect(priceBloc.getAllPrices(), isA<Future<ApiResponse>>());
      });

      test('EMPTY LIST', () async {
        final response = Future.value(
          ApiResponse(status: true, data: <Price>[]),
        );

        when(priceBloc.priceRepository.getAllPrices())
            .thenAnswer((_) => response);

        expect(priceBloc.getAllPrices(), isA<Future<ApiResponse>>());
      });

      test('ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(priceBloc.priceRepository.getAllPrices())
            .thenAnswer((_) => response);

        expect(priceBloc.getAllPrices(), isA<Future<ApiResponse>>());
      });
    });

    group('GET PRICE BY ID', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: price),
        );

        when(priceBloc.priceRepository.getPriceById(1))
            .thenAnswer((_) => response);

        expect(priceBloc.getPriceById(1), isA<Future<ApiResponse>>());
      });

      test('NOT FOUND/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(priceBloc.priceRepository.getPriceById(1))
            .thenAnswer((_) => response);

        expect(priceBloc.getPriceById(1), isA<Future<ApiResponse>>());
      });
    });

    group('STORE PRICE', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, data: price),
        );

        when(priceBloc.priceRepository.storePrice(priceToCreate))
            .thenAnswer((_) => response);

        expect(priceBloc.storePrice(priceToCreate), isA<Future<ApiResponse>>());
      });

      test('NOT CREATED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(priceBloc.priceRepository.storePrice(priceToCreate))
            .thenAnswer((_) => response);

        expect(priceBloc.storePrice(priceToCreate), isA<Future<ApiResponse>>());
      });
    });

    group('UPDATE PRICE', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Updated'),
        );

        when(priceBloc.priceRepository.updatePrice(price))
            .thenAnswer((_) => response);

        expect(priceBloc.updatePrice(price), isA<Future<ApiResponse>>());
      });

      test('NOT FOUND', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Updated'),
        );

        when(priceBloc.priceRepository.updatePrice(priceToCreate))
            .thenAnswer((_) => response);

        expect(
            priceBloc.updatePrice(priceToCreate), isA<Future<ApiResponse>>());
      });

      test('NOT UPDATED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(priceBloc.priceRepository.updatePrice(price))
            .thenAnswer((_) => response);

        expect(priceBloc.updatePrice(price), isA<Future<ApiResponse>>());
      });
    });

    group('DELETE PRICE', () {
      test('OK', () async {
        final response = Future.value(
          ApiResponse(status: true, message: 'Deleted'),
        );

        when(priceBloc.priceRepository.deletePrice(1))
            .thenAnswer((_) => response);

        expect(priceBloc.deletePrice(1), isA<Future<ApiResponse>>());
      });

      test('NOT DELETED/ERROR', () async {
        final response = Future.value(
          ApiResponse(status: false, message: 'Error'),
        );

        when(priceBloc.priceRepository.deletePrice(1))
            .thenAnswer((_) => response);

        expect(priceBloc.deletePrice(1), isA<Future<ApiResponse>>());
      });
    });

    test('SINGLE METHODS', () {
      priceBloc.setPrice(price);
      priceBloc.setPriceList([price, price]);

      expect(priceBloc.getPrice(), isA<Price>());
      expect(priceBloc.getPriceList(), isA<List<Price>>());

      expect(priceBloc.priceListStream, isA<Stream<List<Price>>>());
      expect(priceBloc.priceStream, isA<Stream<Price>>());

      priceBloc.dispose();
    });
  });
}
